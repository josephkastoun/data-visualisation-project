const express = require('express');
const router = express.Router();
const csvParser = require('papaparse');
const fs = require('fs');
const path = require("path");
const csvWriter = require("csv-writer").createArrayCsvWriter;

let processedFiles = [];
let processingPaths = [];

const DEFAULT_OSU = 1755;
const DEFAULT_FILE_NUMBER = "0000000000";
const DEFAULT_PROCESSED_DIR = './dataSets';
const DEFAULT_UNPROCESSED_DIR = './unprocessedDataSets/';

//TODO implement api call to refresh the data sets

/* GET users listing. */
router.get('/', function (req, res, next) {

    const osu = req.query.osu === undefined ? DEFAULT_OSU : req.query.osu;
    const number = req.query.number === undefined ? DEFAULT_FILE_NUMBER : req.query.number;

    if(req.query.countData !== undefined && req.query.countData === 'true'){
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.write(getDataPath(osu).toString());
        res.end();
        return;
    }

    if(req.query.getDirectories !== undefined && req.query.getDirectories === 'true'){
        collectDirectories(DEFAULT_PROCESSED_DIR).then(result => {
            res.writeHead(200, {'Content-Type': 'text/plain'});
            res.write(result.toString());
            res.end();
        });
        return;
    }

    if(req.query.updateData !== undefined && req.query.updateData === 'true'){
        processFiles(DEFAULT_UNPROCESSED_DIR, true);
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.write("Data is being updated.");
        res.write("\n\n\Please wait 5 seconds, then refresh client page.");
        res.end();
        return;
    }

    if(req.query.id !== undefined && req.query.id === 'true'){
        printCSVToScreen(collectIDFile(osu), res);
        return;
    }

    if(req.query.footInfo !== undefined && req.query.footInfo === 'true'){
        printCSVToScreen(findFootInformation(osu), res);
        return;
    }

    printCSVToScreen(getDataPath(osu, number), res);
});

/*
getDataPath(osu, number)

osu (int) is osu number for the file
number (int) is the number of file

returns data path

 */

function getDataPath(osu, number=-1) {
    let finalPath = "nan";

    for (const dir of fs.readdirSync(DEFAULT_PROCESSED_DIR, {withFileTypes: true})) {
        if (dir.isDirectory() && dir.name.includes("OSU") && dir.name.includes(osu.toString())) {
            fs.readdirSync(DEFAULT_PROCESSED_DIR + "/" + dir.name).forEach((files) => {
                const fileCount = (DEFAULT_FILE_NUMBER + number).slice(-DEFAULT_FILE_NUMBER.length).toString();
                if(number === -1 && files.includes(".wad")){
                    if(finalPath === "nan")
                        finalPath = 1;
                    else
                        finalPath++;
                }
                else if (files.includes(fileCount)) {
                    finalPath = DEFAULT_PROCESSED_DIR + "/" + dir.name + "/" + files;
                }
            });
        }
    }

    if (finalPath === "nan") {
        console.log("ERR: File Name not found!");
        return undefined;
    }

    return finalPath;
}

function findFootInformation(osu){
    let dir2 = "";
    for (const dir of fs.readdirSync(DEFAULT_PROCESSED_DIR, {withFileTypes: true})) {
        if (dir.isDirectory() && dir.name.includes("OSU") && dir.name.includes(osu.toString())) {
            fs.readdirSync(DEFAULT_PROCESSED_DIR + "/" + dir.name).forEach((files) => {
                if(files.includes("dat")){
                    dir2 = DEFAULT_PROCESSED_DIR + "/" + dir.name + "/" + files;
                }
            });
        }
    }
    return dir2;
}

function collectIDFile(osu){
    let dir2 = "";
    for (const dir of fs.readdirSync(DEFAULT_PROCESSED_DIR, {withFileTypes: true})) {
        if (dir.isDirectory() && dir.name.includes("OSU") && dir.name.includes(osu.toString())) {
            fs.readdirSync(DEFAULT_PROCESSED_DIR + "/" + dir.name).forEach((files) => {
                if(files.includes("id")){
                    dir2 = DEFAULT_PROCESSED_DIR + "/" + dir.name + "/" + files;
                }
            });
        }
    }
    return dir2;
}


/*

printCSVToScreen(filePath, res)

filePath is the file path as a string
res is the request's res

* Will check if processed file exists. if so it will send it as a JSON response
* If it doesn't exist it will check if the process is currently processing a file with the same path, if so it will
  recursively call the function instead of processing simultaneously
* If the files does exist, it will push the path to be processed, reading the file and returning a JSON file to push to
screen
* After it will add to the processedFiles array and remove the entry from the currently processing files array

 */
function printCSVToScreen(filePath, res) {

    if (filePath === undefined) {
        res.send('<h1> INVALID FILE (CHECK OSU/NUMBER) </h1> ');
        return;
    }

    const prevFile = processedFiles.find( pFile => {
       if(pFile.path === filePath){
           res.send(pFile.file);
           return true;
       }
    });

    if(prevFile != null) {
        return;
    }

    if(processingPaths.includes(filePath)){
        printCSVToScreen(filePath, res);
        return;
    }

    processingPaths.push(filePath);

    let CSV_File = [];

    fs.createReadStream(filePath).pipe(csvParser.parse(csvParser.NODE_STREAM_INPUT, {
        header: !filePath.includes(".dat")
    })).on('data', (row) => {

        if(filePath.includes(".dat") && !filePath.includes(".id")){
            row = {
                dataID: Number(row[0].slice(-14).slice(0, 10)),
                footID: Number(row[1]),
                mistake: Number(row[3])
            };
        }else {
            for (const key of Object.keys(row)) {
                row[key] = Number(row[key]);
            }
        }

        CSV_File.push(row);
    }).on('end', (file) => {
        //console.log(CSV_File);
        processedFiles.push({path : filePath, file : CSV_File});
        processingPaths = processingPaths.filter(((value, index) =>  (value !== filePath)));

        res.send(CSV_File);
        //console.log('CSV file successfully processed: ' + filePath);
    });
}


// Will async read the directory and and locate each file to be processed, then will prepare the file with prepareFile().
function processFiles(dir, val=false) {
    fs.readdir(dir, {withFileTypes: true}, ((err, files) => {
        if (err) throw err;

        for (const res of files) {
            if (res.isDirectory()) {
                processFiles(dir + res.name + "/");
            }
            if (res.isFile() && (res.name.includes("wad") || (res.name.includes("id") && !res.name.includes("old")) || (res.name.includes("dat")))) {
                prepareFile(dir + res.name);
            }
        }
    }));
    //console.log("dir complete: " + dir);
}

function collectDirectories(dir) {
    return new Promise((resolve => {
        let directories = [];
        fs.readdir(dir+"/", {withFileTypes: true}, ((err, files) => {
            if (err) throw err;

            for (const res of files) {
                if (res.isDirectory()) {
                    let v = res.name.replace(/0/g, "").replace("OSU", "");
                    if(v.length === 3){
                        v = v + 0;
                    }
                    directories.push(v);
                }
            }

            resolve(directories);
        }));
    }));
}

function prepareFile(filePath) {
    const splitPath = filePath.split('/');

    const newDir = DEFAULT_PROCESSED_DIR + splitPath.map(((value, index) => {

        if(filePath.includes("id")){
            if (index > 1) return '/'+value;
        }
        else if(filePath.includes("dat")){
            if(index === 2)
                return "/" + value + "/"+ value + ".dat";
        }
        else {
            const subDirectory = splitPath[splitPath.length - 2];

            if (value !== subDirectory && value.includes(subDirectory) && splitPath.length > 1 && subDirectory.includes("OSU")) {
                value = value.replace(subDirectory, "-");
            }
            if (index > 1) return '/' + value;
        }
    })).join('').replace(/\s/g, "");


    if (fs.existsSync(newDir)) return;

    fs.readFile(filePath, 'utf8', (err, data) => {
        if (err) throw err;

        //console.log(data.split("\r\n").map(b => b.split(" = ")[1]).map((a, i) => a + ", " + i));
        //Debug ID File

        if (!createDir(newDir)) {
            prepareFile(filePath);
            return;
        }

        if(filePath.includes("id")){
            const csvWrite = csvWriter({
                header: ['ID', 'FOOTLENGTH', 'BODYMASS'],
                path: newDir
                }
            );
            console.log(filePath);
            const values = data.split("\r\n").map(b => b.split(" = ")[1]);
            if(values.length > 8) {
                const records = [
                    [values[1].replace(/(['"])/g, ""), ((Number(values[8]) + Number(values[10])) / 2), values[12]]
                ];
                csvWrite.writeRecords(records).then(r => {
                    console.log(newDir + " created");
                    }
                );
            }
            return;
        }

        if(filePath.includes("dat")){
            fs.writeFile(newDir, removeLines(data, [0,1,2,3,4,5,6,7,8,9]), 'utf8', (err1, result) => {
                if (err1) throw err1;
                console.log("File Processed: " + newDir);
            });
            return;
        }

        fs.writeFile(newDir, removeLines(data, [0, 1, 2, 3, 4]), 'utf8', (err1, result) => {
            if (err1) throw err1;
            //console.log("File Processed: " + newDir);
        });
    })
}

function createDir(filePath) {
        const dirName = path.dirname(filePath);
        if (fs.existsSync(dirName)) {
            return true;
        }
        if (!fs.existsSync(dirName)){
            fs.mkdirSync(dirName);
        }
}

const removeLines = (data, lines = []) => {
    return data
        .split('\n')
        .filter((val, idx) => lines.indexOf(idx) === -1)
        .join('\n');
};

processFiles(DEFAULT_UNPROCESSED_DIR, true);
setTimeout(() => {
    console.log("Files Processed, Please run the client")}, 6000);

module.exports = router;
