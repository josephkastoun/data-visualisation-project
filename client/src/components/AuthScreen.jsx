import React, {Component} from 'react';

class AuthScreen extends Component {

    constructor(props){
        super(props);
        this.state = {
            loggedIn : false
        }
    }

    render() {
        return (
            <div>
                <div id={"upForm"}>
                    <label className={"upLabel"} for={"username"}>Username</label>
                    <input className={"upForm"} type={"text"} placeholder={"Enter Username"} name={"username"} required/>
                    <label className={"upLabel"} for={"password"}>Password</label>
                    <input className={"upForm"} type={"password"} placeholder={"Enter Password"} name={"password"} required/>

                    <button className={"upButton"} type="submit">Login</button>
                </div>
            </div>
        );
    }
}

export default AuthScreen;