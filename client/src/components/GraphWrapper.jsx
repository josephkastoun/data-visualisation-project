import React, {Component} from 'react';
import './GraphWrapper.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
    LineChart,
    XAxis,
    Line,
    Tooltip,
    YAxis,
    ReferenceArea,
    CartesianGrid, Label, Legend
} from "recharts";
import {Helmet} from "react-helmet";

import IconFile from "./IconFile";

const COLOURS = [
    '#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6',
    '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
    '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A',
    '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
    '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC',
    '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
    '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680',
    '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
    '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3',
    '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'
];

const DEFAULT_MAX_VALUES = 300;
const DEFAULT_MARGIN_SCALAR = 0.96;
const DEFAULT_MARGIN_PERCENTAGE = (1 - DEFAULT_MARGIN_SCALAR) * 100;

class GraphWrapper extends Component {

    constructor(props) {
        super(props);
        this.state = {
            sportData: [],
            averagedData: [],
            resize: {width: 0, height: 0},
            lastClick: 0,
            isFullscreen: false,
            currentDataSelection: 0,
            startMargin: 0,
            margin1: 0,
            margin2: 0,
            jumpValue: 0,
            maxData: DEFAULT_MAX_VALUES,
            width: "40vw",
            height: "40vh",
            showExtraProperties: false,
            selectedGraph : 0,
            PARAMS: [
                {'Fx': true},
                {'Fy': false},
                {'Fz': false},
                {'Mx': false},
                {'My': false},
                {'Mz': false},
                {'IN0': false},
                {'IN1': false},
                {'IN2': false},
                {'IN3': false},
                {'IN4': false},
                {'IN5': false},
            ]
        };

        Object.keys(props).forEach(value => {
            if (this.state.hasOwnProperty(value)) {
                this.state[value] = props[value];
            }
        });

        window.addEventListener("orientationchange", this.updateWindowDimensions);
    }

    updateSportData = () => {
        if(this.props.user === undefined || this.props.user.dataCollected === false){
            setTimeout(this.updateSportData, 200);
            return;
        }
        this.setState({sportData : this.props.user.userData[this.state.selectedGraph]});
        this.setState({averagedData : this.averageData(this.state.sportData, this.state.maxData)});
    };

    componentDidMount() {
        this.getEnabledComponents();
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
        setTimeout(this.updateWindowDimensions, 50);
        this.updateSportData();
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.user.selectedGraph !== this.state.selectedGraph) {
            this.setState({selectedGraph : this.props.user.selectedGraph});
            setTimeout(this.updateSportData, 50);
        }

        Object.keys(this.props).forEach(value => {
        if(this.props[value] !== prevProps[value])
            this.setState({[value] : this.props[value]})
        });
    }

    updateWindowDimensions = () => {
        this.setState({resize: {width: window.innerWidth, height: window.innerHeight}});
    };

    averageData = (data, max = this.state.maxData) => {

        if(data === undefined) return;

        if (max === 0) return data;

        const margin1 = Math.floor(this.state.margin1);
        const margin2 = Math.floor(this.state.margin2);

        if (Math.max(margin1, margin2) - Math.min(margin1, margin2) !== 0) {
            data = data.slice(Math.min(margin1, margin2), Math.max(margin1, margin2));
        }

        const total = data.length;
        const jump = Math.floor(total / max);

        if (jump <= 0) return data;

        let newDataSet = [];

        newDataSet.push(data[0]);

        for (let i = jump; i < total; i += jump) {

            let avgConstructor = {};


            for (let j = i - jump; j < i; j++) {

                if (data[j] === undefined) {
                    avgConstructor["undefined"] = true;
                    break;
                }

                Object.keys(data[j]).forEach((value, index) => {
                    if (avgConstructor.hasOwnProperty(value)) {
                        avgConstructor[value] = data[j][value] + avgConstructor[value];
                    } else {
                        avgConstructor[value] = data[j][value];
                    }
                });
            }

            if (avgConstructor["undefined"]) {
                break;
            }

            Object.keys(avgConstructor).forEach((value, index) => {
                if (value === 'key') {
                    avgConstructor[value] = Math.floor(avgConstructor[value] / jump);
                } else {
                    avgConstructor[value] = avgConstructor[value] / jump;
                }
            });

            newDataSet.push(avgConstructor);
        }

        newDataSet.push(data[data.length - 1]);

        this.setState({jumpValue: jump});

        return newDataSet;
    };

    mouseOnClickEvent = (e) => {
        if (e === null || e.activeLabel === undefined) return;
        this.updateWindowDimensions();
        if (performance.now() - this.state.lastClick < 600) {
            this.setState({startMargin: e.activeLabel});
        }

        this.setState({lastClick: performance.now()});
    };

    mouseOnReleaseEvent = (e) => {
        if (e === null || e.activeLabel === undefined || this.state.startMargin === 0) return;
        this.setState({
            margin2: e.activeLabel,
            margin1: this.state.startMargin,
            startMargin: 0
        });

        setTimeout(() => {
            this.setState({averagedData: this.averageData(this.state.sportData)})
        }, 200)
    };

    onMouseMove = (e) => {
        if (e === null || e.activeLabel === undefined) return;
        this.setState({testData: e.activeLabel})
    };

    onChangeCheckBox = (event, id) => {
        let newValues = this.state.PARAMS;
        newValues.forEach(((value, index) => {
            if (value[id] != null) {
                value[id] = !(value[id]);
            }
        }));
        this.setState({PARAMS: newValues});
    };

    onChangeNumberBox = (event) => {
        this.setState({maxData: Number(event.target.value)});
    };

    getEnabledComponents = () => {

        let components = [];
        this.state.PARAMS.forEach((wan) => {
            for (let [key, value] of Object.entries(wan)) {
                components.push(
                    <div key={key + "checkboxDiv"} className="graphCheckboxDiv">
                        <label className="checkBoxLabel">{key}</label>
                        <input className="checkBox" key={key + "-checkbox"} type='checkbox' defaultChecked={value}
                               onChange={(event => this.onChangeCheckBox(event, key))}/>
                    </div>
                )
            }
        });

        return components;
    };

    toggleExtraProperties = () => {
        this.setState({showExtraProperties: !(this.state.showExtraProperties)});
    };

    extraProperties = () => {
        if (this.state.showExtraProperties) {
            return (
                <div>

                    <div className="graphExtraProperties">
                        <IconFile
                            key={"propertyToggle"}
                            className={this.state.isFullscreen ? "propertyToggle-fs" : "propertyToggle"}
                            onClick={this.toggleExtraProperties}
                            imgURL="assets/settings.svg"
                            hoverURL="assets/settings-hover.svg"
                            alt="settings icon"/>
                    </div>

                    <div className="border rounded py-3 graphExtraPropertiesExpanded">

                        <IconFile key={"propertyToggleExpanded"}
                                  className="propertyToggleExpanded"
                                  imgURL="assets/close.svg"
                                  hoverURL="assets/close-hover.svg"
                                  onClick={this.toggleExtraProperties}
                                  alt="close icon"/>

                        <div className="graphOptionsContainer">
                            {this.getEnabledComponents().map((comp) => comp)}
                        </div>
                        <div key="changeNumberBox" className="changeNumberDiv">
                            <label className="numberInputLabel">Number of data points displayed:</label>
                            <input className="numberBox" type='number' onChange={this.onChangeNumberBox}
                                   value={this.state.maxData}/>
                            <input className="numberSlider" type='range' min='100' max='8000'
                                   onChange={this.onChangeNumberBox}
                                   defaultValue={this.state.maxData}/>
                            <button className='numberSubmitButton'
                                    onClick={() => this.setState({averagedData: this.averageData(this.state.sportData)})}>Update
                            </button>
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="graphExtraProperties">
                    <IconFile
                        key={"propertyToggle"}
                        className={this.state.isFullscreen ? "propertyToggle-fs" : "propertyToggle"}
                        onClick={this.toggleExtraProperties}
                        imgURL="assets/settings.svg"
                        hoverURL="assets/settings-hover.svg"
                        alt="settings icon"/>
                </div>
            )
        }
    };

    toggleFullScreen = () => {
        this.setState({isFullscreen: !(this.state.isFullscreen)});
    };

    getGraphWidth = () => {
        if (this.state.isFullscreen) {
            return this.state.resize.width * DEFAULT_MARGIN_SCALAR;
        }
        if((this.state.resize.width * (Number(this.state.width.replace("vw", "")) / 100)) <= 250){
            return 250;
        }
        if (this.state.width.toLowerCase().includes("vw")) {
            return (this.state.resize.width * (Number(this.state.width.toString().replace("vw", "")) / 100)) * DEFAULT_MARGIN_SCALAR;
        }
        return this.state.width * DEFAULT_MARGIN_SCALAR;
    };

    getGraphHeight = () => {
        if(this.state.resize.height < 250){
            return 135;
        }
        if (this.state.isFullscreen) {
            return this.state.resize.height * DEFAULT_MARGIN_SCALAR;
        }
        if (this.state.height.toLowerCase().includes("vh")) {
            return (this.state.resize.height * (Number(this.state.height.replace("vh", "")) / 100)) * DEFAULT_MARGIN_SCALAR;
        }
        return this.state.height * DEFAULT_MARGIN_SCALAR;
    };

    render() {
        return (
            this.state.sportData !== undefined && this.state.sportData.length > 0 && <div className={this.state.isFullscreen ? "graphDiv-fs" : "graphDiv"}
                 style={{
                     width: this.getGraphWidth(),
                     height: this.getGraphHeight(),
                     margin: (DEFAULT_MARGIN_PERCENTAGE * 0.25) + "%"
                 }}>
                <Helmet>
                    <meta name="viewport"
                          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0"/>
                </Helmet>
                {this.extraProperties()}
                <IconFile key={"setGraphFullscreen"}
                          className={this.state.isFullscreen ? "setGraphFullscreen-fs" : "setGraphFullscreen"}
                          imgURL="assets/fullscreen.svg"
                          hoverURL="assets/fullscreen-hover.svg"
                          onClick={this.toggleFullScreen}
                          alt="fullscreen icon"/>


                <LineChart
                    className={this.state.isFullscreen ? "lineChartInternal-fs": "lineChartInternal"}
                    data={this.state.averagedData}
                    margin={{top: 5, right: 20, left: 0, bottom: 10}}
                    onMouseDown={this.mouseOnClickEvent}
                    onMouseUp={this.mouseOnReleaseEvent}
                    onMouseMove={this.onMouseMove}
                    width={this.getGraphWidth() * 0.95}
                    height={this.getGraphHeight() * 0.95}
                >
                    <Legend align="left" layout="vertical"/>

                    {this.getGraphWidth() > 250 && <YAxis/>}

                    <Tooltip/>

                    {this.state.PARAMS.map((sel, index) => {
                        const data = Object.entries(sel);
                        if (data[0][1]) {
                            return <Line key={index + "-line"} type="monotone" dataKey={data[0][0]}
                                         stroke={COLOURS[index]}/>;
                        } else
                            return null;
                    })}

                    <XAxis dataKey="key">
                        {this.getGraphWidth() > 320 && <Label value={"Each data point = AVG: " + this.state.jumpValue + " points"}
                               position="bottom" offset={-5}/>}
                    </XAxis>

                    <CartesianGrid/>

                    {this.state.startMargin !== 0 && (
                        <ReferenceArea x1={this.state.testData} x2={this.state.startMargin}/>)}
                </LineChart>
            </div>
        );
    }

}

export default GraphWrapper;