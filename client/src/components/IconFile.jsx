import React, {Component} from 'react';

class IconFile extends Component {
    state = {
        currURL: this.props.imgURL,
        imgURL: this.props.imgURL,
        hoverURL: this.props.hoverURL,
        alt: this.props.alt
    };

    updateState = () => {
        if (this.state.currURL !== this.state.imgURL) {
            this.setState({currURL: this.state.imgURL})
        }
    };

    updateStateHover = () => {
        if (this.state.currURL !== this.state.hoverURL) {
            this.setState({currURL: this.state.hoverURL})
        }
    };


    render() {
        return (
            <img
                className={this.props.className}
                src={this.state.currURL}
                alt={this.state.alt}
                onMouseEnter={this.updateStateHover}
                onMouseLeave={this.updateState}
                onTouchStart={this.updateStateHover}
                onTouchEnd={this.updateState}
                onClick={this.props.onClick}
            />
        );
    }
}

export default IconFile;