import React, {Component} from 'react';
import UserSelect from "./UserSelection/UserSelect";
import "./DataSelectionScreen.css"
import User from "./UserSelection/User";
import GraphContainer from "./GraphScreens/GraphContainer";
import lowPassFilter from 'low-pass-filter';


class DataSelectionScreen extends Component {

    arrays = [[], [], [], []];

    constructor(props) {
        super(props);
        this.state = {
            user1: new User(1755),
            user2: new User(1756),
            users: [1755],
            userObjects: {},
            selectionMode: false,
            selectedGraph: "Fx",
            selectedGraphContainer: -1,
            selectedDataItems: []
        };

        new Promise(((resolve, reject) => {
            fetch(`${this.getIpAddress()}/sportData?getDirectories=true`).catch(e => {
            }).then((results) => {
                return results.text();
            }).then(e => {
                resolve(e.split(",").map(a => Number(a)));
            });
        })).then(res => {
            this.setState({users: res});
            let userObj = {};
            this.state.users.forEach(a => {
                userObj[a] = new User(a);
            });
            this.setState({userObjects: userObj});
            for (let i = 0; i < 4; i++) {
                this.state.userObjects[1755].getData(i).then(a => {
                    this.pushData(a, i, i, this.state.user1.userID, ["Fx", "copXvY", "Mx", "Fz"][i]);
                });
            }
        });
    }

    getIpAddress = () => {
        return window.location.origin.toString().replace("3000", "9000");
    };

    setSelectedGraphID = (v) => {
        this.setState({selectedGraph: v});
    };

    addAllDataSets(graphID, userID) {
        if (this.state.selectedDataItems.find((a) => (a.graphID === graphID && a.userID === userID)) === undefined) {
            this.addDataSet(graphID, userID);
        }

        this.state.selectedDataItems.forEach((value => {
            this.addDataSet(value.graphID, value.userID);
        }));

        this.setState({selectedDataItems: []});
    }

    addDataSet = (graphID, userID) => {
        const frameID = this.state.selectedGraphContainer;

        if (frameID === -1) return;

        if (Object.keys(this.state.userObjects).includes(userID.toString())) {
            this.state.userObjects[userID].getData(graphID).then(a => this.pushData(a, frameID, graphID, userID));
        } else {
            let x = {...this.state.userObjects};
            x[userID] = new User(userID);
            this.setState({userObjects: x});

            x[userID].getData(graphID).then(a => this.pushData(a, frameID, graphID, userID));
        }
    };

    pushData = (data, frameID, graphID, userID, selectedType = null) => {
        let val = null;
        if (selectedType === null) {
            selectedType = this.state.selectedGraph;
        }
        if (frameID === 1 && selectedType !== "copXvY") {
            this.pushData(data, frameID, graphID, userID, "copXvY");
            return;
        }
        if (frameID === 3 && selectedType !== "copLvSAI") {
            this.pushData(data, frameID, graphID, userID, "copLvSAI");
            return;
        }

        const startIndex = data.findIndex(d => d["Fz"] > 4);
        if (selectedType === "copXvY") {
            data = data.slice(startIndex);
            let smoothedData = this.lowPassConstructor(data, ["Mx", "Fz", "My"], 30);
            val = data.map((b, i) => {
                const copX = (-smoothedData.Mx[i]) / smoothedData.Fz[i];
                const copY = (smoothedData.My[i]) / smoothedData.Fz[i];
                return {
                    data: copY,
                    key: copX,
                    index: i,
                    title: selectedType + "-" + graphID + "-" + userID
                }
            });
        } else if (selectedType === "copLvSAI") {
            let user = this.state.userObjects[userID];
            let arr = [];
            user.getAllData().then((a) => {
                user.userData.forEach((value, index) => {
                    if(user.FootData[index].mistake === 0) {
                        arr.push({
                            data: user.COPLength[index],
                            key: user.SAI[index],
                            index: index,
                            title: selectedType + "-" + userID,
                            footID: user.FootData[index].footID
                        });
                    }
                });

                this.arrays[frameID].push(arr);
            });
            return;
        } else {
            data = data.slice(startIndex);
            let smoothData = this.lowPassConstructor(data, [selectedType], 30);
            val = data.map((b, i) => {
                return {
                    data: smoothData[selectedType][i],
                    key: b.key - startIndex,
                    title: selectedType + "-" + graphID + "-" + userID
                }
            });
        }

        if (this.arrays[frameID].find(comp => comp[0].key === val[0].key && comp[0].title === val[0].title) === undefined) {
            this.arrays[frameID].push(val);
        }
    };

    lowPassConstructor = (data, keys, freq = 30) => {
        let cons = {};

        keys.forEach((a, i) => {
            cons[a] = data.map(b => b[a]);
            lowPassFilter(cons[a], freq, 1000, 1);
        });

        return cons;
    };

    removeDataSet = (graphID, index) => {
        this.arrays[graphID].splice(index, 1);
    };

    setSelectionMode = (bool) => {
        this.setState({selectionMode: bool});
    };

    setSelectedGraphContainer = (frame) => {
        this.setState({selectedGraphContainer: frame});
    };

    changeUser = (frame, id, name = "nameless") => {
        this.setState({["user" + frame]: new User(id, name)});
    };

    addToSelectedDataItems = (graphID, userID) => {
        let selectedItems = this.state.selectedDataItems;

        if (this.state.selectedDataItems.find((a) => (a.graphID === graphID && a.userID === userID)) !== undefined) {
            selectedItems = selectedItems.filter(i => (i.graphID === graphID && i.userID !== userID) || i.graphID !== graphID);
            selectedItems.sort((a, b) => a.graphID - b.graphID);
        } else {
            selectedItems.push({userID: userID, graphID: graphID});
            selectedItems.sort((a, b) => a.graphID - b.graphID);
        }

        this.setState({selectedDataItems: selectedItems});
    };

    refreshDataSets = () => {
        return new Promise((rRes, rej) => {
            new Promise(((resolve, reject) => {
                fetch(`${this.getIpAddress()}/sportData?getDirectories=true`).catch(e => {
                }).then((results) => {
                    return results.text();
                }).then(e => {
                    resolve(e.split(",").map(a => Number(a)));
                });
            })).then(res => {
                this.setState({users: res});
                let userObj = {};
                this.state.users.forEach(a => {
                    if(this.state.userObjects[a]){
                        userObj[a] = this.state.userObjects[a];
                    }
                    else
                        userObj[a] = new User(a);
                });
                this.setState({userObjects : userObj});
                rRes(true);
            });
        });
    };

    render() {
        return (
            <div className={"dataSelectionScreen"}>
                <div className={"userSelection"}>
                    <UserSelect user1={this.state.user1}
                                user2={this.state.user2}
                                users={this.state.users}
                                changeUser={this.changeUser}
                                setSelectionMode={(x) => this.setSelectionMode(x)}
                                setSelectedGraphID={(x) => this.setSelectedGraphID(x)}
                                selectGraphContainer={(graphID, userID) => this.addAllDataSets(graphID, userID)}
                                selectedDataItems={this.state.selectedDataItems}
                                addToSelectedDataItems={this.addToSelectedDataItems}
                                refreshData={this.refreshDataSets}
                    />
                </div>
                <div className={"graphContainers"}>
                    <GraphContainer
                        arrays={this.arrays}
                        selectionMode={this.state.selectionMode}
                        selectedContainer={this.state.selectedGraphContainer}
                        selectContainer={(x) => this.setSelectedGraphContainer(x)}
                        removeDataSet={this.removeDataSet}
                    />
                </div>
            </div>
        );
    }
}

export default DataSelectionScreen;