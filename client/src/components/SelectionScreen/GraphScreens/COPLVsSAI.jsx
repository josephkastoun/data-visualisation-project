import React, {Component} from 'react';
import * as d3 from "d3";
import './COPGraphContainer.css'
import * as DataHelper from "./DataHelper";
import CartographyLines from "./GraphComponents/CartographyLines";
import Key from "./GraphComponents/Key";
import CopGraph from "./GraphComponents/COPGraph";
import ToolTip from "./GraphComponents/ToolTip";
import DataSlicer from "./GraphComponents/DataSlicer";

const COLORS = [
    '#FF6633', '#3893a8', '#FF33FF', '#8c8c4d', '#00B3E6',
    '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
    '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A',
    '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
    '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC',
    '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
    '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680',
    '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
    '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3',
    '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'
];
/*
    Average data before fz === 4, subtract it from all the results. (50 points)
 */

const GRAPH_TOLERANCE = 0;

class COPLVsSAI extends Component {
    currentArrayPositions = [];

    constructor(props) {
        super(props);

        this.state = {
            internalWidth: 800,
            internalHeight: 800,
            margin: {top: 50, right: 50, bottom: 50, left: 50},
            height: '100%',
            width: '100%',
            xScale: null,
            yScale: null,
            range: [[0, 0], [0, 0]],
            averageDataSets: [],
            dataSets: [],
            dataExtents: {
                y: [0, 0],
                x: [0, 0]
            },
            updateData: false,
            currentArrayPositions: [],
            graphs: [],
            mouse: [0, 0],
            jump: 0,
            scaleModified : false
        };

        window.addEventListener("resize", this.dynamicResize);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.dynamicResize);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.internalHeight !== this.state.internalHeight || prevState.internalWidth !== this.state.internalWidth) {
            this.resetGraph();
        }

        if (!this.state.updateData && this.state.averageDataSets.length !== this.state.dataSets.length) {
            this.setState({updateData: true});
            this.averageAllArrays();
        }

        if (this.props.dataSets.length === 0 && this.state.averageDataSets.length !== 0) {
            this.setState({averageDataSets: [], dataSets: [], graphs: [], currentArrayPositions: []});
        }

        if (prevProps.dataSets !== this.props.dataSets || (this.props.dataSets.length > 0 && this.state.dataSets.length === 0)) {
            this.setState({dataSets: this.props.dataSets});
            setTimeout(() => {
                this.averageAllArrays();
                this.forceUpdate()
            }, 500);
        }
    }

    componentDidMount() {
        this.svgContainer = this.svg.select(".svgGraphContainer");
        this.dynamicResize();
        this.svg.on("mousemove", this.mouseMoveEvent);
        this.averageAllArrays();
    }

    setCurrentPosition = (id, value) => {

        if (value == null) {
            this.resetCurrentPositions();
            return;
        }

        this.currentArrayPositions[id] = value;

        if (this.currentArrayPositions !== this.state.currentArrayPositions)
            this.setState({currentArrayPositions: this.currentArrayPositions});
    };

    resetCurrentPositions = () => {
        this.currentArrayPositions = [];
    };

    mouseMoveEvent = () => {
        if (this.svg == null) return;

        this.setState({mouse: d3.mouse(this.svg.node())});
        this.drawXAxisLine(d3.mouse(this.svg.node()));
    };

    getMinMaxDataSets = () => {

        if(this.state.range[0][0] !== this.state.range[0][1] || this.state.range[1][0] !== this.state.range[1][1]){
            this.setState({
                dataExtents: {
                    y: this.state.range[1],
                    x: this.state.range[0]
                }, scaleModified : true
            });
            this.drawAxis();
            return;
        }

        let yMin = Number.MAX_SAFE_INTEGER;
        let yMax = Number.MIN_SAFE_INTEGER;
        let xMin = Number.MAX_SAFE_INTEGER;
        let xMax = Number.MIN_SAFE_INTEGER;

        if (this.state.averageDataSets) {
            this.state.averageDataSets.forEach((dataSet) => {
                if (Promise.resolve(dataSet) !== dataSet) {
                    const extent = this.minMaxDataSet(dataSet.map(a => a.data));
                    const range = this.minMaxDataSet(dataSet.map(a => a.key));

                    if (extent[0] < yMin)
                        yMin = extent[0];
                    if (extent[1] > yMax)
                        yMax = extent[1];
                    if (range[0] < xMin)
                        xMin = range[0];
                    if (range[1] > xMax)
                        xMax = range[1];
                }
            });

            if (yMin !== Number.MAX_SAFE_INTEGER && xMin !== Number.MAX_SAFE_INTEGER)
                this.setState({
                    dataExtents: {
                        y: [1, 4],
                        x: [20, 100]
                    }, scaleModified : false
                });
        }


        this.drawAxis();
    };

    minMaxDataSet = (data) => {
        return d3.extent(data);
    };

    averageAllArrays = () => {
        if (this.state.dataSets.length === 0) {
            setTimeout(() => {
                this.averageAllArrays()
            }, 700);
            return;
        }

        let dataSets = [];
        this.state.dataSets.forEach((dataSet, i) => {
            const d = this.averageDataSet(i);
            dataSets.push(d);
        });


        Promise.all(dataSets).then(values => {
            this.setState({averageDataSets: values});
            this.getMinMaxDataSets();
            this.setState({updateData: false});
        });
    };

    averageDataSet = (i) => {
        if (this.state.dataSets.length < i) return null;
        return new Promise(resolve => resolve(this.state.dataSets[i].filter(this.withinRange)));
    };

    withinRange = (data) => {
        const x = data["key"];
        const y = data["data"];
        const xCons = this.state.range[0];
        const yCons = this.state.range[1];

        return !
            ((xCons != null && xCons[0] !== xCons[1] && (x < xCons[0] || x > xCons[1])) ||
                (yCons != null && yCons[0] !== yCons[1] && (y < yCons[0] || y > yCons[1])));
    };

    setRangeX = (x) => {
        if (x[0] === x[1]) {
            this.setRanges([0, 0]);
            return;
        }

        this.setRanges(d3.extent([this.mouseToGraphX(x[0], true), this.mouseToGraphX(x[1])]));
    };

    setRangeY = (y) => {
        if (y[0] === y[1]) {
            this.setRanges(null, [0,0]);
            return;
        }

        this.setRanges(null, d3.extent([this.mouseToGraphY(y[0], true), this.mouseToGraphY(y[1])]));
    };

    setRanges = (x = null, y = null) => {
        let ranges = [...this.state.range];
        if (x != null) {
            ranges[0] = x;
        }
        if (y != null) {
            ranges[1] = y;
        }

        this.setState({range: ranges});
        this.averageAllArrays();
    };

    dynamicResize = () => {
        let height = 0;
        let width = 0;
        if (this.state.height.includes("%")) {
            let perc = parseFloat(this.state.height) / 100;
            height = this.graphContainer.offsetHeight * perc;
        } else {
            height=  this.state.offsetHeight;
        }
        if (this.state.width.includes("%")) {
            let perc = parseFloat(this.state.width) / 100;
            width = this.graphContainer.offsetWidth * perc;
        } else {
            width = this.state.offsetWidth;
        }

        let final = Math.min(width, height);

        this.setState({internalHeight : final, internalWidth : final});
    };

    resetGraph = () => {
        if (this.svgContainer != null)
            this.svgContainer.selectAll("*").remove();

        this.svgContainer = this.svg.select(".svgGraphContainer");
        this.drawAxis();
    };

    mouseToGraphX = (x, noNegatives = false) => {
        if (noNegatives) {
            if (x + GRAPH_TOLERANCE < this.state.margin.left || this.state.xScale == null)
                return 0;
        } else {
            if (x + GRAPH_TOLERANCE < this.state.margin.left || this.state.xScale == null)
                return -1;
            if (x - GRAPH_TOLERANCE > this.state.internalWidth - this.state.margin.right)
                return -2;
        }

        return this.state.xScale.invert(x - this.state.margin.left);
    };

    mouseToGraphY = (y) => {
        return (this.state.yScale.invert(y));
    };


    drawAxis = format => {
        if (this.svgContainer == null) {
            this.resetGraph();
            this.drawAxis();
            return;
        }

        this.svgContainer.selectAll(".graphAxis").remove();

        const xScale = d3.scaleLinear()
            .domain(this.state.dataExtents.x)
            .range([0, this.state.internalWidth - this.state.margin.right - this.state.margin.left]);

        this.svgContainer.append("svg")
            .attr("class", "graphAxis")
            .attr("x", this.state.margin.left)
            .attr("y", this.state.margin.top)
            .attr("height", this.state.internalHeight - this.state.margin.top - this.state.margin.bottom)
            .attr("width", this.state.internalWidth - this.state.margin.left - this.state.margin.right);

        this.svgContainer.append("g")
            .attr("class", "graphAxis")
            .attr("transform", `translate(${this.state.margin.left}, ${this.state.internalHeight - this.state.margin.bottom})`)
            .call(d3.axisBottom(xScale));

        const yScale = d3.scaleLinear()
            .domain(this.state.dataExtents.y)
            .range([this.state.internalHeight - this.state.margin.bottom, this.state.margin.top]);


        this.setState({xScale: xScale, yScale: yScale});

        this.svgContainer.append("g")
            .attr("class", "graphAxis")
            .attr("transform", `translate(${this.state.margin.left}, 0)`)
            .call(d3.axisLeft(yScale));

        this.svgContainer.append("text")
            .attr("class", "graphAxis")
            .attr("x", this.state.internalWidth / 2)
            .attr("y", this.state.internalHeight - (this.state.margin.bottom * 0.3))
            .attr("text-anchor", "middle")
            .text("Shock attenuation index")
            .attr("font-family", "sans-serif")
            .attr("font-size", "12px")
            .attr("color", "black");

        this.svgContainer.append("text")
            .attr("class", "graphAxis")
            .attr("x", 10)
            .attr("y", this.state.internalHeight / 2)
            .attr("text-anchor", "middle")
            .text("COP Length")
            .attr("font-family", "sans-serif")
            .attr("font-size", "12px")
            .attr("color", "black")
            .attr("writing-mode", "tb");
    };

    formatTicks = (extent, maxTicks = 15) => {
        let ticks = [];
        const min = extent[0];
        let max = extent[1];
        const spread = (max - min) / (maxTicks - 1);

        for (let i = 0; i < maxTicks; i++) {
            ticks.push(min + i * spread);
        }

        return ticks;
    };

    roundToD(num, e) {
        if (num === 0)
            return 0;
        return +(Math.round(num + `e+${e}`) + `e-${e}`);
    }

    drawXAxisLine = (mousePos) => {
        if (mousePos[0] < this.state.margin.left + 2 ||
            mousePos[0] > this.state.internalWidth + 2 - this.state.margin.right ||
            mousePos[1] > this.state.internalHeight - this.state.margin.bottom ||
            mousePos[1] < this.state.margin.top) {
            if (this.xAxisLine != null)
                this.xAxisLine.remove();
            if (this.yAxisLine != null)
                this.yAxisLine.remove();
            this.xAxisLine = null;
            this.yAxisLine = null;
            return;
        }

        if (this.xAxisLine != null) {
            this.xAxisLine
                .attr("x1", mousePos[0])
                .attr("y1", this.state.margin.top)
                .attr("x2", mousePos[0])
                .attr("y2", this.state.internalHeight - this.state.margin.bottom);
        } else {
            this.xAxisLine = this.svg.append("line")
                .attr("x1", mousePos[0])
                .attr("y1", 5)
                .attr("x2", mousePos[0])
                .attr("y2", 50)
                .attr("stroke-width", 1)
                .attr("stroke", "black");
        }

        if (this.yAxisLine != null) {
            this.yAxisLine
                .attr("x1", this.state.margin.left)
                .attr("y1", mousePos[1])
                .attr("x2", this.state.internalWidth - this.state.margin.right)
                .attr("y2", mousePos[1]);
        } else {
            this.yAxisLine = this.svg.append("line")
                .attr("x1", this.state.margin.left)
                .attr("y1", mousePos[1])
                .attr("x2", this.state.internalWidth - this.state.margin.right)
                .attr("y2", mousePos[1])
                .attr("stroke-width", 1)
                .attr("stroke", "black");
        }
    };

    convertMouseX = (x) => {
        if (x + GRAPH_TOLERANCE < this.state.margin.left) {
            return -1;
        }
        if (x - GRAPH_TOLERANCE > this.state.internalWidth - this.state.margin.right) {
            return -2;
        }

        return x;
    };

    convertMouseY = (y) => {
        if (y + GRAPH_TOLERANCE < this.state.margin.top) {
            return -1;
        }
        if (y - GRAPH_TOLERANCE > this.state.internalHeight - this.state.margin.bottom) {
            return -2;
        }

        return y;
    };


    render() {
        return (
            <div className={"COPGraphSurround"}>
                <div id={"copGraph" + this.props.id}
                     className={"D3COPGraphContainer"}
                     ref={ele => this.graphContainer = ele}
                     onMouseEnter={this.props.onMouseEnter}
                     onMouseLeave={this.props.onMouseLeave}
                     style={this.props.highlight ? {background: "#61892F"} : {}}
                >
                    <svg width={this.graphContainer != null ? this.graphContainer.offsetWidth:this.state.internalWidth}
                         height={this.graphContainer != null ? this.graphContainer.offsetHeight:this.state.internalHeight}
                         ref={element => (this.svg = d3.select(element))}
                         className={"d3Graph"}
                         viewBox={`0 0 ${this.graphContainer != null ? this.graphContainer.offsetWidth:this.state.internalWidth} ${this.graphContainer != null ? this.graphContainer.offsetHeight:this.state.internalHeight}`}
                    >
                        <g className={"svgGraphContainer"}>

                        </g>

                        <DataSlicer margin={this.state.margin}
                                    mouse={[this.convertMouseX(this.state.mouse[0]), this.convertMouseY(this.state.mouse[1])]}
                                    internalHeight={this.state.internalHeight}
                                    internalWidth={this.state.internalWidth}
                                    setRangeX={this.setRangeX}
                                    setRangeY={this.setRangeY}
                                    id={this.props.id}
                                    graphName={"copGraph"}
                                    containerName={"D3COPGraphContainer"}
                        />

                        <CartographyLines
                            xScale={this.state.xScale}
                            yScale={this.state.yScale}
                            margin={this.state.margin}
                            height={this.state.internalHeight}
                            width={this.state.internalWidth}
                            dataExtents={this.state.dataExtents}
                            formatTicks={this.formatTicks}
                        />

                        <Key keys={this.state.dataSets.length === 0 ? [] : this.state.dataSets.map(a => a[0].title)}
                             COLORS={COLORS}
                             position={{x: this.state.internalWidth - this.state.margin.right + 10, y: this.state.margin.top + 5}}
                             sideDisplay={true}
                             removeDataSet={(i) => {
                                 this.props.removeDataSet(this.props.id, i);
                             }
                             }/>

                        {
                            this.state.averageDataSets.map(((value, index) => {
                                return <CopGraph margin={this.state.margin}
                                                 altColor={true}
                                                 vsSAI={true}
                                                 scaleModified={this.state.scaleModified}
                                                 xScale={this.state.xScale}
                                                 yScale={this.state.yScale}
                                                 mouse={this.mouseToGraphX(this.convertMouseX(this.state.mouse[0]))}
                                                 mouseY={this.mouseToGraphY(this.convertMouseY(this.state.mouse[1]))}
                                                 data={value}
                                                 id={index}
                                                 key={index}
                                                 setCurrentPosition={this.setCurrentPosition}
                                                 resetCurrentPositions={this.resetCurrentPositions}
                                                 color={COLORS[index]}
                                />
                            }))
                        }

                        <ToolTip currentArrayPositions={this.state.currentArrayPositions}
                                 mouse={this.state.mouse}
                                 colors={COLORS}
                                 width={this.state.internalWidth}
                                 height={this.state.internalHeight}
                                 margin={this.state.margin}
                                 xyTooltip={true}
                        />

                    </svg>
                </div>
            </div>
        );
    }
}

export default COPLVsSAI;