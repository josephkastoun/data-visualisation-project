import React, {Component} from 'react';
import * as d3 from "d3";
import './D3GraphContainer.css'
import DataSlicer from "./GraphComponents/DataSlicer";
import * as DataHelper from "./DataHelper";
import LineGraph from "./GraphComponents/LineGraph";
import ToolTip from "./GraphComponents/ToolTip";
import CartographyLines from "./GraphComponents/CartographyLines";
import Key from "./GraphComponents/Key";

const COLORS = [
    '#FF6633', '#FFB399', '#FF33FF', '#8c8c4d', '#00B3E6',
    '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
    '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A',
    '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
    '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC',
    '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
    '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680',
    '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
    '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3',
    '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'
];

const GRAPH_TOLERANCE = 0;

class D3GraphContainer extends Component {
    currentArrayPositions = [];

    constructor(props) {
        super(props);

        this.state = {
            internalWidth: 800,
            internalHeight: 500,
            margin: {top: 10, right: 40, bottom: 60, left: 40},
            height: '100%',
            width: '100%',
            xScale: null,
            yScale: null,
            range: [0, 0],
            averageDataSets: [],
            dataSets: [],
            dataExtents: {
                y: [0, 0],
                x: [0, 0]
            },
            updateData: false,
            currentArrayPositions: [],
            graphs : [],
            mouse: [0, 0],
            jump: 0
        };
        window.addEventListener("resize", this.dynamicResize);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.dynamicResize);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.internalHeight !== this.state.internalHeight || prevState.internalWidth !== this.state.internalWidth) {
            this.resetGraph();
        }

        if (!this.state.updateData && this.state.averageDataSets.length !== this.state.dataSets.length) {
            this.setState({updateData: true});
            this.averageAllArrays();
        }

        if (this.props.dataSets.length === 0 && this.state.averageDataSets.length !== 0){
            this.setState({averageDataSets : [], dataSets : [], graphs : [], currentArrayPositions : []});
        }

        if (prevProps.dataSets !== this.props.dataSets || (this.props.dataSets.length > 0 && this.state.dataSets.length === 0)) {
            this.setState({dataSets: this.props.dataSets});
            setTimeout(() => {
                this.averageAllArrays();
                this.forceUpdate()
            }, 500);
        }
    }

    componentDidMount() {
        this.dynamicResize();
        this.svg.on("mousemove", this.mouseMoveEvent);
        this.averageAllArrays();
    }

    setCurrentPosition = (id, value) => {

        this.currentArrayPositions[id] = value;

        if (this.currentArrayPositions !== this.state.currentArrayPositions)
            this.setState({currentArrayPositions: this.currentArrayPositions});
    };

    resetCurrentPositions = () => {
      this.currentArrayPositions = [];
    };

    mouseMoveEvent = () => {
        if(this.svg == null) return;

        this.setState({mouse: d3.mouse(this.svg.node())});
        this.drawXAxisLine(d3.mouse(this.svg.node()));
    };

    getMinMaxDataSets = () => {
        let yMin = Number.MAX_SAFE_INTEGER;
        let yMax = Number.MIN_SAFE_INTEGER;
        let xMin = Number.MAX_SAFE_INTEGER;
        let xMax = Number.MIN_SAFE_INTEGER;

        if (this.state.averageDataSets) {
            this.state.averageDataSets.forEach((dataSet) => {
                if (Promise.resolve(dataSet) !== dataSet) {
                    const extent = this.minMaxDataSet(dataSet.map(a => a.data));
                    const range = this.minMaxDataSet(dataSet.map(a => a.key));

                    if (extent[0] < yMin)
                        yMin = extent[0];
                    if (extent[1] > yMax)
                        yMax = extent[1];
                    if (range[0] < xMin)
                        xMin = range[0];
                    if (range[1] > xMax)
                        xMax = range[1];
                }
            });

            if (yMin !== Number.MAX_SAFE_INTEGER && xMin !== Number.MAX_SAFE_INTEGER)
                this.setState({
                    dataExtents: {
                        y: [yMin, yMax],
                        x: [xMin, xMax]
                    }
                });
        }

        this.drawAxis();
    };

    minMaxDataSet = (data) => {
        return d3.extent(data);
    };

    averageAllArrays = () => {
        if (this.state.dataSets.length === 0) {
            setTimeout(() => {
                this.averageAllArrays()
            }, 700);
            return;
        }

        let dataSets = [];
        this.state.dataSets.forEach((dataSet, i) => {
            const d = this.averageDataSet(i);
            dataSets.push(d);
        });

        Promise.all(dataSets).then(values => {
            const d = values.map(a => {
                this.setState({jump: a.jump});
                return a.data;
            });

            this.setState({averageDataSets: d});
            this.getMinMaxDataSets();
            this.setState({updateData: false});

            let graphArr = [];

            this.state.averageDataSets.forEach(((value, index) => {
                 graphArr.push(<LineGraph margin={this.state.margin}
                                  xScale={this.state.xScale}
                                  yScale={this.state.yScale}
                                  mouse={this.mouseToGraph(this.convertMouse(this.state.mouse[0]))}
                                  data={value}
                                  id={index}
                                  key={index}
                                  setCurrentPosition={this.setCurrentPosition}
                                  color={COLORS[index]}
                />)
            }));

            this.setState({graphs : graphArr});

        });
    };

    averageDataSet = (i) => {
        if (this.state.dataSets.length < i) return null;
        if (this.state.range[0] !== this.state.range[1]) {
            return DataHelper.averageData(this.state.dataSets[i].slice(this.state.range[0], this.state.range[1])).then(result => result);
        }
        return DataHelper.averageData(this.state.dataSets[i]).then(result => {
            return result;
        });
    };

    setRange = (x) => {
        if (x[0] === x[1]) {
            this.setState({range: [0, 0]});
            this.averageAllArrays();
            return;
        }
        const values = d3.extent([x[0], x[1]]);

        this.setState({range: [this.mouseToGraph(values[0], true), this.mouseToGraph(values[1])]});
        this.averageAllArrays();
    };

    dynamicResize = () => {
        if (this.state.height.includes("%")) {
            let perc = parseFloat(this.state.height) / 100;
            this.setState({internalHeight: this.graphContainer.offsetHeight * perc})
        } else {
            this.setState({internalHeight: this.state.offsetHeight})
        }
        if (this.state.width.includes("%")) {
            let perc = parseFloat(this.state.width) / 100;
            this.setState({internalWidth: this.graphContainer.offsetWidth * perc})
        } else {
            this.setState({internalWidth: this.state.offsetWidth})
        }
    };

    resetGraph = () => {
        if (this.svgContainer != null)
            this.svgContainer.remove();

        this.svgContainer = this.svg.append("g").attr("class", "svgGraphContainer");
        this.drawAxis();
    };

    mouseToGraph = (x, noNegatives = false) => {
        if(noNegatives){
            if (x + GRAPH_TOLERANCE < this.state.margin.left || this.state.xScale == null)
                return 0;
        } else {
            if (x + GRAPH_TOLERANCE < this.state.margin.left || this.state.xScale == null)
                return -1;
            if (x - GRAPH_TOLERANCE > this.state.internalWidth - this.state.margin.right)
                return -2;
        }

        return Math.round(this.state.xScale.invert(x - this.state.margin.left));
    };

    drawAxis = format => {
        if (this.svgContainer == null) {
            this.resetGraph();
            this.drawAxis();
            return;
        }

        this.svgContainer.selectAll(".graphAxis").remove();

        const xScale = d3.scaleLinear()
            .domain(this.state.dataExtents.x)
            .range([0, this.state.internalWidth - this.state.margin.right - this.state.margin.left]);

        this.svgContainer.append("g")
            .attr("class", "graphAxis")
            .attr("transform", `translate(${this.state.margin.left}, ${this.state.internalHeight - this.state.margin.bottom})`)
            .call(d3.axisBottom(xScale).tickValues(this.formatTicks(this.state.dataExtents.x)));

        const yScale = d3.scaleLinear()
            .domain(this.state.dataExtents.y)
            .range([this.state.internalHeight - this.state.margin.bottom, this.state.margin.top]);


        this.setState({xScale: xScale, yScale: yScale});

        this.svgContainer.append("g")
            .attr("class", "graphAxis")
            .attr("transform", `translate(${this.state.margin.left}, 0)`)
            .call(d3.axisLeft(yScale).tickValues(this.formatTicks(this.state.dataExtents.y)).tickFormat(a => this.roundToD(a, 1)));

        if (this.state.averageDataSets.length <= 0) return;

        this.svgContainer.selectAll(".bottomText").remove();

        this.svgContainer.append("text")
            .attr("class", "bottomText")
            .attr("x", this.state.internalWidth / 2)
            .attr("y", this.state.internalHeight - (this.state.margin.bottom * 0.3))
            .attr("text-anchor", "middle")
            .text("Data Points per X: " + this.state.jump)
            .attr("font-family", "sans-serif")
            .attr("font-size", "15px")
            .attr("color", "black");
    };

    formatTicks = (extent, maxTicks = 15) => {
        let ticks = [];
        const min = extent[0];
        let max = extent[1];
        const spread = (max - min) / (maxTicks - 1);

        for (let i = 0; i < maxTicks; i++) {
            ticks.push(min + i * spread);
        }

        return ticks;
    };

    roundToD(num, e) {
        if(num === 0)
            return 0;
        return +(Math.round(num + `e+${e}`) + `e-${e}`);
    }

    drawXAxisLine = (mousePos) => {
        if (mousePos[0] < this.state.margin.left + 2 || mousePos[0] > this.state.internalWidth + 2 - this.state.margin.right || mousePos[1] > this.state.internalHeight - 5 || mousePos[1] < 5) {
            if (this.xAxisLine != null)
                this.xAxisLine.remove();
            this.xAxisLine = null;
            return;
        }

        if (this.xAxisLine != null) {
            this.xAxisLine
                .attr("x1", mousePos[0])
                .attr("y1", this.state.margin.top)
                .attr("x2", mousePos[0])
                .attr("y2", this.state.internalHeight - this.state.margin.bottom);

            return;
        }

        this.xAxisLine = this.svg.append("line")
            .attr("x1", mousePos[0])
            .attr("y1", 5)
            .attr("x2", mousePos[0])
            .attr("y2", 50)
            .attr("stroke-width", 1)
            .attr("stroke", "black");
    };

    convertMouse = (x) => {
        if (x + GRAPH_TOLERANCE < this.state.margin.left) {
                return -1;
            }
        if (x - GRAPH_TOLERANCE > this.state.internalWidth - this.state.margin.right) {
            return -2;
        }

        return x;
    };

    render() {
        return (
            <div className={"LineGraphSurround"}>
                <div id={"lineGraph" + this.props.id}
                     className={"D3LineGraphContainer"}
                     ref={ele => this.graphContainer = ele}
                     onMouseEnter={this.props.onMouseEnter}
                     onMouseLeave={this.props.onMouseLeave}
                     style={this.props.highlight ? {background : "#61892F"}:{}}
                >
                    <svg width={this.state.internalWidth}
                         height={this.state.internalHeight}
                         ref={element => (this.svg = d3.select(element))}
                         className={"d3Graph"}>

                        <CartographyLines
                            xScale={this.state.xScale}
                            yScale={this.state.yScale}
                            margin={this.state.margin}
                            height={this.state.internalHeight}
                            width={this.state.internalWidth}
                            dataExtents={this.state.dataExtents}
                            formatTicks={this.formatTicks}
                        />

                        <Key keys={this.state.dataSets.length === 0 ? [] : this.state.dataSets.map(a => a[0].title)}
                             COLORS={COLORS}
                             position={{x: 10, y: this.state.internalHeight - this.state.margin.bottom + 30}}
                             removeDataSet={(i) => {
                                this.props.removeDataSet(this.props.id, i);
                             }
                         }/>

                        <DataSlicer margin={this.state.margin}
                                    mouse={[this.convertMouse(this.state.mouse[0])]}
                                    internalHeight={this.state.internalHeight}
                                    internalWidth={this.state.internalWidth}
                                    setRangeX={this.setRange}
                                    id={this.props.id}
                                    graphName={"lineGraph"}
                                    containerName={"D3LineGraphContainer"}
                        />

                        {
                            this.state.averageDataSets.map(((value, index) => {
                                return <LineGraph margin={this.state.margin}
                                                  xScale={this.state.xScale}
                                                  yScale={this.state.yScale}
                                                  mouse={this.mouseToGraph(this.convertMouse(this.state.mouse[0]))}
                                                  data={value}
                                                  id={index}
                                                  key={index}
                                                  setCurrentPosition={this.setCurrentPosition}
                                                  resetCurrentPositions={this.resetCurrentPositions}
                                                  color={COLORS[index]}
                                />
                            }))
                        }

                        <ToolTip currentArrayPositions={this.state.currentArrayPositions} mouse={this.state.mouse}
                                 colors={COLORS} width={this.state.internalWidth} height={this.state.internalHeight}
                                 margin={this.state.margin}/>
                    </svg>
                </div>
            </div>
        );
    }
}

export default D3GraphContainer;