export function averageData(data, max = 450){
    return new Promise(((resolve, reject) => {
        if(data === undefined) reject("no data!");

        if (max === 0) {
            resolve({data: data, jump: 1});
            return;
        }

        const total = data.length;
        const jump = Math.floor(total / max);

        if (jump <= 0) {
            resolve({data: data, jump: 1});
            return;
        }

        let newDataSet = [];

        newDataSet.push(data[0]);

        for (let i = jump; i < total; i += jump) {

            let avgConstructor = {};

            for (let j = i - jump; j < i; j++) {

                if (data[j] === undefined) {
                    avgConstructor["undefined"] = true;
                    break;
                }

                Object.keys(data[j]).forEach((value, index) => {
                    if (avgConstructor.hasOwnProperty(value)) {
                        avgConstructor[value] = data[j][value] + avgConstructor[value];
                    } else {
                        avgConstructor[value] = data[j][value];
                    }
                });
            }

            if (avgConstructor["undefined"]) {
                break;
            }

            Object.keys(avgConstructor).forEach((value, index) => {
                if (value === 'key') {
                    avgConstructor[value] = avgConstructor[value] / jump;
                } else {
                    avgConstructor[value] = (avgConstructor[value] / jump);
                }
            });

            newDataSet.push(avgConstructor);
        }

        newDataSet.push(data[data.length - 1]);

        resolve({data: newDataSet, jump: jump});
    }));}
