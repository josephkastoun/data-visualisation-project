import React, {Component} from 'react';
import * as d3 from 'd3';


let ivfg = 0;

class CopGraph extends Component {

    LightenDarkenColor(col, amt) {

        let usePound = false;

        if (col[0] === "#") {
            col = col.slice(1);
            usePound = true;
        }

        const num = parseInt(col, 16);

        let r = (num >> 16) + amt;

        if (r > 255) r = 255;
        else if (r < 0) r = 0;

        let b = ((num >> 8) & 0x00FF) + amt;

        if (b > 255) b = 255;
        else if (b < 0) b = 0;

        let g = (num & 0x0000FF) + amt;

        if (g > 255) g = 255;
        else if (g < 0) g = 0;

        return (usePound ? "#" : "") + (g | (b << 8) | (r << 16)).toString(16);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.xScale !== this.props.xScale || prevProps.yScale !== this.props.yScale || prevProps.data !== this.props.data) {
            this.lastUpdate = 0;
            this.drawGraph();
            this.drawLineCircles();
        }

        this.drawCurrentMouse();
    }

    componentWillUnmount() {
        this.props.resetCurrentPositions();
    }

    drawLineCircles = () => {
        console.log(this.props.data.length);
        if (this.circles != null && this.circlesArray != null){
            console.log("beans" + this.props.data.length);

            let newData = this.props.data;

            if(this.props.vsSAI && !this.props.scaleModified){
                const avgKey = this.props.data.reduce((a, b) => a + Number(b["key"]),0) / this.props.data.length;
                const avgData = this.props.data.reduce((a, b) => a + Number(b["data"]), 0) / this.props.data.length;
                newData = [...this.props.data, {key : avgKey, data: avgData}];
            }

            this.circlesArray.interrupt();

            this.circlesArray = this.circles
                .selectAll("circle")
                .data(newData.filter(d => (d.hasOwnProperty("footID") && d.footID === 0) || !d.hasOwnProperty("footID")));

            this.circlesArray
                .enter()
                .append("circle")
                .attr("cx", (value) => this.props.xScale(value["key"]) + this.props.margin.left)
                .attr("cy", (value) => this.props.yScale(value["data"]));

            this.circlesArray
                .attr("cx", (value) => this.props.xScale(value["key"]) + this.props.margin.left)
                .attr("cy", (value) => this.props.yScale(value["data"]))
                .attr("r", 0);

            this.circles
                .selectAll("circle")
                .transition()
                .delay((d,i) => i * 5)
                .ease(d3.easeExp)
                .attr("r", (value, i) => i === value.key === newData[newData.length-1].key ? 8 : 5)
                .style("fill", this.props.vsSAI ? this.props.color : "black")
                .attr("stroke-width", (value, i) => value.key === newData[newData.length-1].key ? 4 : 0.5)
                .attr("stroke", this.props.black)
                .delay((d,i) => i*5 + 10)
                .transition()
                .style("fill", (value, i) => value.key === newData[newData.length-1].key ? "black" : (this.props.altColor ? this.props.color: this.LightenDarkenColor(this.props.color, -(100/this.props.data.length)*value["index"])))
                .attr("stroke", this.props.color)
                .attr("r", (value, i) => value.key === newData[newData.length-1].key ? 5 : 3.5);

            this.circlesArray.exit().remove();

            if (!this.props.vsSAI) return;

            this.rectArray = this.rects
                .selectAll("rect")
                .data(newData.filter(d => (d.hasOwnProperty("footID") && d.footID === 1)));

            this.rectArray
                .enter()
                .append("rect")
                .attr("x", (value) => this.props.xScale(value["key"]) + this.props.margin.left -3.5)
                .attr("y", (value) => this.props.yScale(value["data"]) -3.5);

            this.rectArray
                .attr("x", (value) => this.props.xScale(value["key"]) + this.props.margin.left -3.5)
                .attr("y", (value) => this.props.yScale(value["data"]) -3.5)
                .attr("width", 0)
                .attr("height", 0);

            this.rects.selectAll("rect")
                .transition()
                .delay((d,i) => i * 5)
                .ease(d3.easeExp)
                .attr("width", 7)
                .attr("height", 7)
                .attr("x", (value) => this.props.xScale(value["key"]) + this.props.margin.left - 3.5)
                .attr("y", (value) => this.props.yScale(value["data"]) - 3.5)
                .style("fill", (value, i) => i === newData.length-1 ? "black" : (this.props.altColor ? this.props.color: this.LightenDarkenColor(this.props.color, -(100/this.props.data.length)*value["index"])))
                .attr("stroke", this.props.color);


            this.rectArray.exit().remove();

            return;
        }

        if (this.props.data == null || Promise.resolve(this.props.data) === this.props.data) return;

        this.circles = this.graph.append("g").attr("class", "circles");

        this.circlesArray = this.circles
            .selectAll("circle")
            .data(this.props.data);

        this.circlesArray
            .attr("cx", (value) => this.props.xScale(value["key"]) + this.props.margin.left)
            .attr("cy", (value) => this.props.yScale(value["data"]));


        if (this.props.vsSAI) {
            this.rects = this.graph.append("g").attr("class", "rectangles");

            this.rectArray = this.rects
                .selectAll("rect")
                .data(this.props.data);

            this.rectArray
                .attr("width", 7)
                .attr("height", 7)
                .attr("x", (value) => this.props.xScale(value["key"]) + this.props.margin.left - 3.5)
                .attr("y", (value) => this.props.yScale(value["data"]) - 3.5)
                .style("fill", (value, i) => (this.props.altColor ? this.props.color: this.LightenDarkenColor(this.props.color, -(100/this.props.data.length)*value["index"])))
                .attr("stroke", this.props.color);
        }

        setTimeout(() => {this.drawLineCircles();}, 0);

    };

    drawGraph = () => {
        if (this.graph == null || this.props.data == null || this.props.data.length === 0 | true) return;

        this.graph.selectAll("path").remove();

        const line = d3.line()
            .x(((d, i) => this.props.xScale(d["key"])))
            .y(((d, index1) => this.props.yScale(d["data"]))).curve(d3.curveNatural);

        this.line = line;

        this.graph.append("path")
            .datum(this.props.data)
            .attr("class", "lineContainer")
            .attr("transform", `translate(${this.props.margin.left}, 0)`)
            .attr("d", line);
    };


    drawCurrentMouse = () => {
        const goal = this.props.mouse;
        const goal2 = this.props.mouseY;

        if(this.props.data == null || Promise.resolve(this.props.data) === this.props.data || this.props.data.length === 0) {
            d3.selectAll(`.activeCircle${this.props.id}`).remove();
            return;
        }


        const value = this.props.data.reduce((prev, curr) =>
            Math.abs(curr["key"] - goal) <= Math.abs(prev["key"] - goal) &&
            Math.abs(curr["data"] - goal2) < Math.abs(prev["data"] - goal2)
                ? curr : prev);

        if(value == null) return;

        if(value["key"] === this.currentCirclePos) return;

        d3.selectAll(`.activeCircle${this.props.id}`).remove();

        this.currentCirclePos = value;

        const x = this.props.xScale(value["key"]);
        const y = this.props.yScale(value["data"]);

        this.props.setCurrentPosition(this.props.id, value);

        this.graph.append("g").attr("class", `activeCircle${this.props.id}`).append(this.props.vsSAI && value.footID === 1 ? "rect" : "circle")
            .attr(this.props.vsSAI && value.footID === 1 ? "x" :"cx", x + this.props.margin.left - (this.props.vsSAI && value.footID === 1 ? 4.5 : 0))
            .attr(this.props.vsSAI && value.footID === 1 ? "y" :"cy", y - (this.props.vsSAI && value.footID === 1 ? 4.5 : 0))
            .attr("width", 9)
            .attr("height", 9)
            .attr("r", 5)
            .style("fill", this.LightenDarkenColor(this.props.color, -50))
            .attr("stroke-width", 1)
            .attr("stroke", this.props.color);
    };

    render() {
        return (
            <g className={"copGraph"} id={"copGraph" + this.props.id} ref={graph => this.graph = d3.select(graph)} style={this.style}>

            </g>
        );
    }
}

CopGraph.defaultProps = {
    margin: {
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    altColor : false,
    vsSAI : false,
    xScale: 0,
    yScale: 0,
    data: []
};

export default CopGraph;