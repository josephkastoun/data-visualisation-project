import React, {Component} from 'react';
import * as d3 from 'd3';
import LineGraph from "./LineGraph";

class CartographyLines extends Component {

    xTicks = [];
    yTicks = [];
    color = "#434343";

    constructor(props) {
        super(props);
        this.drawn = false;
    }

    componentDidMount() {
        this.yLineContainer = this.cartoLines.append("g").attr("class", "yCartoLines");
        this.xLineContainer = this.cartoLines.append("g").attr("class", "xCartoLines");
        setTimeout(this.drawLines, 1000);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(this.props.width !== prevProps.width || this.props.height !== prevProps.height){
            setTimeout(this.drawLines, 1000);
        }
    }

    drawLines = () => {
        if(this.props.dataExtents.x[1] === 0){
            setTimeout(this.drawLines, 1000);
            return;
        }

        this.drawXLines();
        this.drawYLines();
    };

    drawYLines = () => {
        const xTicks = this.props.formatTicks(this.props.dataExtents.y);
        xTicks.forEach(((value, index) => {
            if(this.xTicks[index] === undefined){
                this.xTicks[index] = (this.yLineContainer.append("line")
                    .attr("x1", this.props.margin.left)
                    .attr("x2", this.props.width - this.props.margin.right)
                    .attr("y1", this.props.yScale(value) + 0.5)
                    .attr("y2", this.props.yScale(value) + 0.5)
                    .attr("stroke-width", 0.5)
                    .attr("stroke", this.color))
            }else if(this.xTicks[index].attr("y1") !== (this.props.yScale(value) + 0.5)){
                this.xTicks[index]
                    .attr("y1", this.props.yScale(value) + 0.5)
                    .attr("y2", this.props.yScale(value) + 0.5)
                    .attr("x1", this.props.margin.left)
                    .attr("x2", this.props.width - this.props.margin.right)
            }
        }));
    };

    drawXLines = () => {
        const yTicks = this.props.formatTicks(this.props.dataExtents.x);
        yTicks.forEach(((value, index) => {
            if(this.yTicks[index] === undefined){
                this.yTicks[index] = (this.yLineContainer.append("line")
                    .attr("x1", this.props.margin.left + this.props.xScale(value) + 0.5)
                    .attr("x2", this.props.margin.left + this.props.xScale(value) + 0.5)
                    .attr("y1", this.props.margin.top)
                    .attr("y2", this.props.height - this.props.margin.bottom)
                    .attr("stroke-width", 0.5)
                    .attr("stroke", this.color))
            }else if(this.yTicks[index].attr("y1") !== (this.props.yScale(value) + 0.5)){
                this.yTicks[index]
                    .attr("x1", this.props.margin.left + this.props.xScale(value) + 0.5)
                    .attr("x2", this.props.margin.left + this.props.xScale(value) + 0.5)
                    .attr("y1", this.props.margin.top)
                    .attr("y2", this.props.height - this.props.margin.bottom)
            }
        }));
    };

    render() {
        return (
            <g className={"cartoLines"} ref={cartoLines => this.cartoLines = d3.select(cartoLines)} style={this.style}>

            </g>
        );
    }
}


LineGraph.defaultProps = {
    xScale : d3.scaleLinear(),
    yScale : d3.scaleLinear(),
    dataExtents :  {
        y: [0, 0],
        x: [0, 0]
    },
    formatTicks : () => {}
};

export default CartographyLines;