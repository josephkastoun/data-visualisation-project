import React, {Component} from 'react';
import * as d3 from 'd3';

const Default_ARR_Val = -256;

class DataSlicer extends Component {

    style = {
        fill: "#212529"
    };

    constructor(props) {
        super(props);

        this.state = {
            x : [0,0],
            y : [0,0],
            draw: false,
            clicked: false
        };

        window.addEventListener("mousemove", this.onMouseMove);
    }

    componentWillUnmount() {
        d3.select("#" + this.props.graphName + this.props.id).on("mousedown", null);
    }

    hasYAxisSlice = () =>{
      return this.props.setRangeY != null;
    };

    setX(x1=Default_ARR_Val,x2=Default_ARR_Val){
        let newX = [...this.state.x];

        if(x1 !== Default_ARR_Val){
            newX[0] = x1;
        }

        if(x2 !== Default_ARR_Val){
            newX[1] = x2;
        }

        this.setState({x : newX});
    }

    setY(y1=Default_ARR_Val,y2=Default_ARR_Val){
        let newY = [...this.state.y];

        if(y1 !== Default_ARR_Val){
            newY[0] = y1;
        }

        if(y2 !== Default_ARR_Val){
            newY[1] = y2;
        }

        this.setState({y : newY});
    }

    mouseDownXEvent = () => {
        this.setX(this.props.mouse[0], this.props.mouse[0]);
    };

    mouseMoveXEvent = () => {
        if(this.hasYAxisSlice()){
            const x = this.state.x[0];
            const y = this.state.y[0];

            let positions = [this.props.mouse[0] - x, this.props.mouse[1]-y];
            let direction = [positions[0] < 0 ? -1 : 1, positions[1] < 0 ? -1 : 1];

            let posDistance = 0;
            let x2 = x;
            let y2 = y;

            if(direction[0] === direction[1]){
                direction[1] = 1;
            } else {
                direction[1] = -1;
            }

            if(x + positions[0] >= this.props.margin.left &&
                x + positions[0] <= this.props.internalWidth - this.props.margin.right &&
                y + (positions[0] * direction[1]) >= this.props.margin.top &&
                y + positions[0] * direction[1] <= this.props.internalHeight - this.props.margin.bottom
            ){
                posDistance = Math.abs(positions[0]);
                x2 = x+positions[0];
                y2 = y+(positions[0] * direction[1]);
            }

            if(direction[0] === direction[1]){
                direction[0] = 1;
            } else {
                direction[0] = -1;
            }

            direction = [positions[0] < 0 ? -1 : 1, positions[1] < 0 ? -1 : 1];

            if(x + positions[1] * direction[0] >= this.props.margin.left &&
                x + positions[1] * direction[0] <= this.props.internalWidth - this.props.margin.right &&
                y + (positions[1]) >= this.props.margin.top &&
                y + positions[1] <= this.props.internalHeight - this.props.margin.bottom
            ){
                if(posDistance < Math.abs(positions[1])){
                    x2 = x+(positions[1] * direction[1]);
                    y2 = y+(positions[1]);
                }
            }

            this.setX(x, x2);
            this.setY(y, y2);

            return;
        }
        if (this.props.mouse[0] === -1) {
            this.setX(Default_ARR_Val, 0);
        } else if (this.props.mouse[0] === -2) {
            this.setX(Default_ARR_Val, this.props.internalWidth - this.props.margin.right);
        } else
            this.setX(Default_ARR_Val, this.props.mouse[0]);
    };

    mouseUpXEvent = () => {
        if (this.state.x[0] !== this.state.x[1]) {
            if (this.state.x[1] === -1) {
                this.setX(Default_ARR_Val, 0);
            } else if (this.state.x[1] === -2) {
                this.setX(Default_ARR_Val, this.props.internalWidth - this.props.margin.right);
            }
        }

        this.props.setRangeX([this.state.x[0], this.state.x[1]]);
    };

    mouseDownYEvent = () => {
        if(!this.hasYAxisSlice()) return;

        this.setY(this.props.mouse[1], this.props.mouse[1]);
    };

    mouseUpYEvent = () => {
        if(!this.hasYAxisSlice()) return;

        this.props.setRangeY([this.state.y[0], this.state.y[1]]);
    };


    componentDidMount() {
        d3.select("#"+ this.props.graphName + this.props.id).on("mousedown", () => {
            this.setState({
                draw: true
            });
            this.mouseDownXEvent();
            this.mouseDownYEvent();

            d3.selectAll("." + this.props.containerName).on("mousemove", () => {
                this.mouseMoveXEvent();
            });

            d3.select(window).on("mouseup", () => {

                if (this.state.clicked) {
                    this.setState({clicked: false});
                    if(this.hasYAxisSlice())
                        this.props.setRangeY([0,0]);
                    this.props.setRangeX([0,0]);
                } else {
                    this.setState({clicked: true});
                    setTimeout(() => {
                        this.setState({clicked: false})
                    }, 300);
                }

                this.mouseUpXEvent();
                this.mouseUpYEvent();

                d3.select("#" +this.props.graphName+this.props.id).on("mousemove", null);
                d3.select(window).on("mouseup", null);
                this.setState({draw: false});
            });

        });
    }

    drawBackgroundRect = (x, y) => {
        if (this.rect == null) return;

        this.rect.selectAll("*").remove();

        if (!this.state.draw) return;

        x = d3.extent(x);

        if (x[0] < this.props.margin.left)
            x[0] = this.props.margin.left;
        if (x[1] > this.props.internalWidth - this.props.margin.right)
            x[1] = this.props.internalWidth - this.props.margin.right;

        y = d3.extent(y);
        if(!this.hasYAxisSlice()){
            y[0] = this.props.margin.top;
            y[1] = this.props.internalHeight - this.props.margin.bottom;
        }
        if(x[1] - x[0] <= 0 || y[1] - y[0] <= 0) return;

        this.rect
            .append("rect")
            .attr("x", x[0])
            .attr("y", y[0])
            .attr("width", x[1] - x[0])
            .attr("height", y[1] - y[0]);
    };

    render() {
        return (
            <g className={"backgroundRect"} ref={rec => this.rect = d3.select(rec)} style={this.style}>
                {this.drawBackgroundRect(this.state.x, this.state.y)}
            </g>
        );
    }
}

DataSlicer.defaultProps = {
    mousePosition: [0, 0],
    margin: {
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    internalHeight: 0,
    internalWidth : 0,
    setRangeX : null,
    setRangeY : null,
    setMargins: (x1, x2) => {
    }
};

export default DataSlicer;