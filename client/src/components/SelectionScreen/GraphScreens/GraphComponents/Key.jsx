import React, {Component} from 'react';
import * as d3 from 'd3';

class Key extends Component {

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(!this.compareArrays(this.props.keys, prevProps.keys) ||
            this.props.position.x !== prevProps.position.x ||
            this.props.position.y !== prevProps.position.y || this.props.keys.length === 0){
            this.drawKeys();
        }
    }

    compareArrays = (a1, a2) => {
        if(a1.length !== a2.length) return false;

        a1.forEach((val,i) => {
            if(val !== a2[i]){
                return false;
            }
        });

        return true;
    };

    componentDidMount() {
        this.drawKeys();
    }

    render() {
        return (
            <g className={"graphKey"} ref={g => this.g = d3.select(g)} style={this.style}>

            </g>
        );
    }

    drawKeys() {
        if(this.props.keys.length === 0){
            this.g.selectAll("keyRect")
                .on('click', null)
                .on('mouseover', null)
                .on('mouseout', null);

            this.g.selectAll(".keyText").remove();
            this.g.selectAll(".keyRect").remove();
            return;
        }

        this.g.selectAll("keyRect")
            .on('click', null)
            .on('mouseover', null)
            .on('mouseout', null);

        this.g.selectAll(".keyText").remove();
        this.g.selectAll(".keyRect").remove();

        const spacing = Math.max(...this.props.keys.map(a => a.toString().length)) * 6 + 8;

        this.props.keys.forEach((value, index) => {

            let x = this.props.position.x;
            let yOff = 0;

            if(!this.props.sideDisplay){
                if(index > 2){
                    x += spacing * (Math.floor(index/3));
                    yOff = 30 * (Math.floor(index/3));
                }
            }

            this.g
                .append("rect")
                .attr("class", "keyRect")
                .attr("id", "keyRect" + index)
                .attr("x", x)
                .attr("y", this.props.position.y - yOff - 8 + 10*index)
                .attr("width", 8)
                .attr("height", 8)
                .attr("fill", this.props.COLORS[index])
                .on("click", () => {
                    this.props.removeDataSet(index);
                    this.g.select("#keyRect"+index).on("click", null)
                        .on('mouseover', null)
                        .on('mouseout', null);
                })
                .on('mouseover', () => {
                    this.g.select("#keyRect" + index).attr("stroke-width", "1px").attr("stroke", "black");
                })
                .on('mouseout', () => {
                    this.g.select("#keyRect" + index).attr("stroke-width", "0").attr("stroke",);
                });

            this.g.append("text")
                .attr("class", "keyText")
                .attr("x", x + 9)
                .attr("y", this.props.position.y - yOff + 10*index)
                .text(value)
                .attr("font-family", "sans-serif")
                .attr("font-size", "10px")
                .attr("color", "black");
        });
    }
}

Key.defaultProps = {
    position : {x:0, y:0},
    keys : ["Fx", "Fy"],
    COLORS : [],
    sideDisplay : false
};

export default Key;