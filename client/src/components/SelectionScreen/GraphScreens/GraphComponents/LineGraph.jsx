import React, {Component} from 'react';
import * as d3 from 'd3';

class LineGraph extends Component {

    constructor(props) {
        super(props);
        this.lastUpdate = 0;
    }

    LightenDarkenColor(col, amt) {

        let usePound = false;

        if (col[0] === "#") {
            col = col.slice(1);
            usePound = true;
        }

        const num = parseInt(col, 16);

        let r = (num >> 16) + amt;

        if (r > 255) r = 255;
        else if (r < 0) r = 0;

        let b = ((num >> 8) & 0x00FF) + amt;

        if (b > 255) b = 255;
        else if (b < 0) b = 0;

        let g = (num & 0x0000FF) + amt;

        if (g > 255) g = 255;
        else if (g < 0) g = 0;

        return (usePound ? "#" : "") + (g | (b << 8) | (r << 16)).toString(16);
    }

    style = {
        fill: 'none',
        stroke: this.props.color,
        strokeWidth: 2,
    };

    componentDidMount() {
        setTimeout(this.drawLineCircles, 500);
    }

    componentWillUnmount() {
        this.props.resetCurrentPositions();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.xScale !== this.props.xScale || prevProps.yScale !== this.props.yScale || prevProps.data !== this.props.data) {
            this.lastUpdate = 0;
            this.drawGraph();
            this.drawLineCircles();
        }

        this.drawCurrentMouse();
    }

    drawCurrentMouse = () => {
        const goal = this.props.mouse;

        if(this.props.data == null || Promise.resolve(this.props.data) === this.props.data || this.props.data.length === 0) return;

        const value = this.props.data.reduce((prev, curr) => Math.abs(curr["key"] - goal) < Math.abs(prev["key"] - goal) ? curr : prev);

        d3.select(`.activeCircle${this.props.id}`).remove();

        if(goal < 0) {
            return;
        }

        const x = this.props.xScale(value["key"]);
        const y = this.props.yScale(value["data"]);

        this.props.setCurrentPosition(this.props.id, value);

        this.graph.append("g").attr("class", `activeCircle${this.props.id}`).append("circle")
            .attr("cx", x + this.props.margin.left)
            .attr("cy", y)
            .attr("r", 5)
            .style("fill", this.LightenDarkenColor(this.props.color, -50))
            .attr("stroke-width", 1)
            .attr("stroke", this.props.color);
    };


    drawLineCircles = () => {


        if (this.circles != null && this.circlesArray != null){

            this.circlesArray.interrupt();

            this.circlesArray = this.circles
                .selectAll("circle")
                .data(this.props.data);

            this.circlesArray
                .enter()
                .append("circle")
                .attr("class", "lineGraphCircle")
                .attr("r", 3.5)
                .style("fill", this.LightenDarkenColor(this.props.color, 30))
                .attr("stroke-width", 1)
                .attr("stroke", this.props.color)
                .attr("cx", (value) => this.props.xScale(value["key"]) + this.props.margin.left)
                .attr("cy", (value) => this.props.yScale(value["data"]));;

            this.circlesArray.transition()
                .duration(400)
                .ease(d3.easePoly)
                .attr("cx", (value) => this.props.xScale(value["key"]) + this.props.margin.left)
                .attr("cy", (value) => this.props.yScale(value["data"]));

            this.circlesArray.exit().remove();

            return;
        }

        if (this.props.data == null || Promise.resolve(this.props.data) === this.props.data) return;


        this.circles = this.graph.append("g").attr("class", "circles");
        this.circlesArray = this.circles
            .selectAll("circle")
            .data(this.props.data)
            .enter()
            .append("circle")
            .attr("class", "lineGraphCircle")
            .attr("cx", (value) => this.props.xScale(value["key"]) + this.props.margin.left)
            .attr("cy", (value) => this.props.yScale(value["data"]))
            .attr("r", 3.5)
            .style("fill", this.LightenDarkenColor(this.props.color, 30))
            .attr("stroke-width", 1)
            .attr("stroke", this.props.color);
    };


    drawGraph = () => {
        if (this.graph == null || this.props.data == null || this.props.data.length === 0) return;

        if(this.lineGraph){
            const line = d3.line()
                .x(((d, i) => this.props.xScale(d["key"])))
                .y(((d, index1) => this.props.yScale(d["data"])))
                .curve(d3.curveLinear);
            this.lineGraph
                .datum(this.props.data)
                .attr("transform", `translate(${this.props.margin.left}, 0)`);
                this.lineGraph.transition().duration(400).ease(d3.easePoly).attr("d", line);
            return;
        }
        this.graph.selectAll("path").remove();


        const line = d3.line()
            .x(((d, i) => this.props.xScale(d["key"])))
            .y(((d, index1) => this.props.yScale(d["data"])))
            .curve(d3.curveLinear);

        this.line = line;

        this.lineGraph = this.graph.append("path")
            .datum(this.props.data)
            .attr("class", "lineContainer")
            .attr("transform", `translate(${this.props.margin.left}, 0)`)
            .attr("d", line);

        this.lastUpdate = Date.now();
    };


    render() {
        return (
            <g className={"lineGraph"} id={"lineGraph" + this.props.id} ref={graph => this.graph = d3.select(graph)} style={this.style}>

            </g>
        );
    }
}

LineGraph.defaultProps = {
    margin: {
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    xScale: 0,
    yScale: 0,
    data: []
};

export default LineGraph;