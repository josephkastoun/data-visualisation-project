import React, {Component} from 'react';
import * as d3 from 'd3';

let WIDTH = 120;

class ToolTip extends Component {

    childObjects = [];
    isVisible = false;

    componentDidUpdate(prevProps, prevState, snapshot) {

        if(this.tooltip && this.tooltip.nodes().length === 0 && this.props.currentArrayPositions.length > 0){
            if(this.rect != null)
                this.rect.remove();
            this.rect = null;
            if(this.indexPosition != null)
                this.indexPosition.remove();
            this.indexPosition = null;
            this.childObjects.forEach(a => a.remove());
        }

        if (this.rect == null || this.rect.offsetParent === null)
            this.rect = this.tooltip.append("rect").attr("class", "toolTipChild")
                .attr("x", this.props.mouse[0])
                .attr("y", this.props.mouse[1])
                .attr("fill", "white")
                .attr("stroke-width", 1)
                .attr("stroke", "black");

        if (this.indexPosition == null || this.indexPosition.offsetParent === null)
            this.indexPosition =
                this.tooltip.append("text")
                    .attr("class", "toolTipChild")
                    .attr("x", 20 + this.props.mouse[0] + 2)
                    .attr("y", this.props.mouse[1] + 15)
                    .attr("font-family", "sans-serif")
                    .attr("font-size", "15px")
                    .attr("text-anchor", "middle")
                    .text("x | y")
                    .attr("fill", "black");

        this.props.currentArrayPositions.forEach(((value, index) => {
            if (this.childObjects[index] === undefined) {
                this.childObjects[index] = this.tooltip.append("text")
                    .attr("class", "toolTipChild")
                    .attr("x", this.outOfBoundsX() ? this.props.mouse[0] + 2 - WIDTH : this.props.mouse[0] + WIDTH/2)
                    .attr("y", this.props.mouse[1] + 20 + (15 * (index + 1)))
                    .text(value["data"])
                    .attr("font-family", "sans-serif")
                    .attr("font-size", "15px")
                    .attr("text-anchor", "middle")
                    .attr("color", this.props.colors[index]);
            }
        }));

        this.drawTooltip();
    }

    drawTooltip = () => {
        if (this.tooltip == null || this.props.currentArrayPositions == null || this.props.currentArrayPositions.length === 0) return;

        if (this.props.mouse[0] + 2 < this.props.margin.left ||
            this.props.mouse[0] - 2 > this.props.width - this.props.margin.right ||
            this.props.mouse[1] < this.props.margin.top ||
            this.props.mouse[1] > this.props.height - this.props.margin.bottom) {

            d3.selectAll(".toolTipChild").remove();

            this.rect = null;
            this.indexPosition = null;
            this.childObjects.length = 0;

            return;
        }

        if (this.props.currentArrayPositions.length > 0) {
            WIDTH = 8 * Math.max(...this.props.currentArrayPositions.map(a => (this.roundToD(a["key"], 3) + " | " + this.roundToD(a["data"], 3) + (a["index"] === undefined ? "" : " | " + a["index"])).toString().length));
            this.childObjects.forEach(((value, index) => {
                if (index + 1 > this.props.currentArrayPositions.length){
                    this.childObjects.splice(index, 1);
                }
                else if (this.props.currentArrayPositions.length > index && this.childObjects[index].text !== value["data"]) {
                    value
                        .attr("fill", this.props.colors[index])
                        .text(this.roundToD(this.props.currentArrayPositions[index]["key"], 3) + " | " + this.roundToD(this.props.currentArrayPositions[index]["data"], 3) + (this.props.currentArrayPositions[index]["index"] === undefined ? "" : " | " + this.props.currentArrayPositions[index]["index"]));

                    value
                        .transition()
                        .duration(50)
                        .attr("x", this.outOfBoundsX() ? this.props.mouse[0] - 12 - WIDTH + (WIDTH/2) : this.props.mouse[0] + 12 + WIDTH/2)
                        .attr("y", this.outOfBoundsY() ? (this.props.height - (25 + 15 * this.props.currentArrayPositions.length + this.props.margin.bottom)) + 20 + (15 * (index + 1)) : this.props.mouse[1] + 20 + (15 * (index + 1)));
                }
            }));
        } else {
            this.childObjects = [];
        }

        this.rect.attr("fill", "white")
            .attr("stroke-width", 1)
            .attr("stroke", "black")
            .attr("height", 25 + 15 * this.props.currentArrayPositions.length)
            .attr("width", WIDTH);

        this.rect
            .transition()
            .duration(50)
            .attr("x", this.outOfBoundsX() ? this.props.mouse[0] - WIDTH - 12 : this.props.mouse[0] + 12)
            .attr("y", this.outOfBoundsY() ? this.props.height - (25 + 15 * this.props.currentArrayPositions.length + this.props.margin.bottom) : this.props.mouse[1]);

        this.indexPosition
            .transition()
            .duration(50)
            .attr("x", this.outOfBoundsX() ? this.props.mouse[0] - 12 - WIDTH + (WIDTH/2) : this.props.mouse[0] + 12 + WIDTH/2)
            .attr("y", this.outOfBoundsY() ? (this.props.height - (25 + 15 * this.props.currentArrayPositions.length + this.props.margin.bottom)) + 15 : this.props.mouse[1] + 15);
    };

    outOfBoundsX = () => {
        return this.props.mouse[0] + WIDTH > this.props.width;
    };

    outOfBoundsY = () => {
        return this.props.mouse[1] + (25 + 15 * this.props.currentArrayPositions.length) > this.props.height - this.props.margin.bottom;
    };

    roundToD(num, places = 3) {
        return +(Math.round(parseFloat(num + "e+" + places)) + "e-" + places);
    }


    render() {
        return (
            <g className={"toolTip"} ref={tooltip => this.tooltip = d3.select(tooltip)} style={this.style}>

            </g>
        );
    }
}

export default ToolTip;