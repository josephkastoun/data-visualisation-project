import React, {Component} from 'react';
import "./GraphContainer.css"
import User from "../UserSelection/User";
import D3GraphContainer from "./D3GraphContainer";
import COPGraphContainer from "./COPGraphContainer";
import COPLVsSAI from "./COPLVsSAI";

class GraphContainer extends Component {

    hoverCSS = {
        backgroundColor : '#f5dd90'
    };

    doContainerSelection = (frame) => {
        this.props.selectContainer(frame);
    };

    componentDidMount() {
        this.user1 = new User(1756);
        this.user2 = new User(1756);
    }

    render() {
        return (
            <div className={"graphContainer"}>
                <div className={"dualGraphContainer"}
                     style={this.props.selectedContainer === 0 && this.props.selectionMode ? this.hoverCSS : {} }
                >
                    <D3GraphContainer dataSets = {this.props.arrays[0]}
                                      onMouseEnter={() => this.doContainerSelection(0)}
                                      onMouseLeave={() => this.doContainerSelection(-1)}
                                      id={0}
                                      highlight={this.props.selectionMode && this.props.selectedContainer === 0}
                                      removeDataSet={this.props.removeDataSet}/>

                    <COPGraphContainer dataSets = {this.props.arrays[1]}
                                      onMouseEnter={() => this.doContainerSelection(1)}
                                      onMouseLeave={() => this.doContainerSelection(-1)}
                                      id={1}
                                      highlight={this.props.selectionMode && this.props.selectedContainer === 1}
                                      removeDataSet={this.props.removeDataSet}
                    />
                </div>
                    <div className={"dualGraphContainer"}
                         style={this.props.selectedContainer === 1 && this.props.selectionMode ? this.hoverCSS : {} }
                    >
                        <D3GraphContainer dataSets = {this.props.arrays[2]}
                                          onMouseEnter={() => this.doContainerSelection(2)}
                                          onMouseLeave={() => this.doContainerSelection(-1)}
                                          id={2}
                                          highlight={this.props.selectionMode && this.props.selectedContainer === 2}
                                          removeDataSet={this.props.removeDataSet}/>

                        <COPLVsSAI dataSets = {this.props.arrays[3]}
                                          onMouseEnter={() => this.doContainerSelection(3)}
                                          onMouseLeave={() => this.doContainerSelection(-1)}
                                          id={3}
                                          highlight={this.props.selectionMode && this.props.selectedContainer === 3}
                                          removeDataSet={this.props.removeDataSet}/>
                </div>
            </div>
        );
    }
}


GraphContainer.defaultProps = {
    user1 : new User(1755),
    user2 : new User(1755),
    selectionMode : false,
    selectedContainer : 0,
    selectContainer : () => {}
};

export default GraphContainer;