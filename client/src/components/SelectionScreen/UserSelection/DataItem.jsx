import React, {Component} from 'react';
import './DataItem.css';

class DataItem extends Component {

    ref = undefined;

    constructor(props) {
        super(props);
        this.state = {
            beingDragged: false,
            selectionMode : false,
            mouseTime : 0,
            width : 0,
            clickOffset : {
                x : 0,
                y: 0
            },
            ignoreClick : false,
            shiftSelected : false
        };

        this.ref = React.createRef();

        if(!this.props.isDraggable) return;

        window.addEventListener('mouseup', this.onMouseUpHandler);
    }

    componentWillUnmount() {
        window.removeEventListener('mouseup', this.onMouseUpHandler);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevState.beingDragged !== this.state.beingDragged){
            this.props.setSelectionMode(this.state.beingDragged);
        }
    }

    customStyle = () => {
        if (this.state.beingDragged) {
            return {
                position: 'absolute',
                top : this.props.mousePosition.y - this.state.clickOffset.y,
                left : this.props.mousePosition.x - this.state.clickOffset.x,
                width : this.state.width,
                zIndex : 5,
                touchAction : 'none',
                pointerEvents : 'none'
            };
        }
        else if(this.props.containsDataElement()){
            return {
                backgroundColor : "#FCD0A1",
                color : "#474B4F"
            };
        }

        else {
            return {
            }
        }
    };

    disableDrag = () => {
      this.setState({beingDragged : false});
    };

    onClickHandler = (e) => {
        this.props.onClick();

        if(Date.now() - this.state.mouseTime > 300){
            this.setState({mouseTime : 0});
            return;
        }

        if(this.state.beingDragged) return;
        this.setState({selectionMode : !this.state.selectionMode});
    };

    onMouseDownHandler = (e) => {
        if(!this.props.isDraggable) return;

        if(e.shiftKey){
            this.props.addToSelectedDataItems();
            return;
        }

        if(this.state.ignoreClick){
            this.setState({ignoreClick : false});
            return;
        }

        if (e.button === 2) return;

        window.addEventListener("contextmenu", this.disableDrag);

        this.setState({width : this.ref.current.offsetWidth,
            beingDragged: true,
            mouseTime : Date.now(),
            clickOffset : {
                x : e.nativeEvent.offsetX,
                y: e.nativeEvent.offsetY
            }
        });
    };

    onTouchDownHandler = (e) => {
        this.setState({ignoreClick : true});
    };

    onMouseUpHandler = (e) => {
        if(this.state.beingDragged && this.props.isDraggable)
            this.props.selectGraphContainer();
        this.setState({beingDragged: false,
            selectionMode : false});

        window.removeEventListener("contextmenu", this.disableDrag);
    };

    selectiveRender = () => {
        if(!this.state.selectionMode){
            return (<li className={"dataItem"}
                        ref={this.ref}
                        style={this.customStyle()}
                        onClick={e => this.onClickHandler(e)}
                        onMouseDown={this.onMouseDownHandler}
                        onTouchStart={this.onTouchDownHandler}
                > {this.props.text} </li>
            );
        } else {
            return (
            <div className={"dataItem"} onClick={this.onClickHandler}>
                <button className={"screenButton"} onClick={this.onButtonPress}>Screen 1</button>
                <button className={"screenButton"} onClick={this.onButtonPress}>Screen 2</button>
            </div>
            )
        }
    };

    render() {
        return (this.selectiveRender());
    }

    onButtonPress = (e) => {
        e.stopPropagation();
    };
}

DataItem.defaultProps = {
    dataSet: [],
    text: "placeholder",
    isDraggable: false,
    beingDragged: false,
    onClick: () => {},
    setSelectionMode : (x) => {}
};

export default DataItem;