import lowPassFilter from "low-pass-filter";
import 'bootstrap/dist/css/bootstrap.min.css';

class User {

    userData = [];
    selectedGraph = 0;

    FzPeak = [];
    FzPeakTime = [];
    SAI = [];
    COPLength = [];
    FootData = [];
    AthleteData = {
        BODYMASS: -1,
        FOOTLENGTH: -1,
        ID: -1
    };


    getIpAddress = () => {
        return window.location.origin.toString().replace("3000", "9000");
    };

    constructor(userID) {
        this.userID = userID;
        this.fetchID();
        this.fetchDataCount();
        this.collectFootData();
    }

    getData(i) {
        return this.fetchDataCount().then(() => new Promise(((resolve, reject) => {
            if (this.userData.length < i+1) reject("too large for array");

            if (this.userData[i].length !== 0) {
                resolve(this.userData[i]);
            }
            let x = this.fetchData(i);
            x.catch((res) => {
                resolve(null);
            }).then((res) => {
                resolve(res)
            });
        })));
    }

    getAllData(){
        return new Promise((resolve => {
            let x = [];
            this.userData.forEach((a, i) => {
                x.push(this.getData(i));
            });
            Promise.all(x).then((res) => {
                this.userData = this.userData.filter(a => !Array.isArray(a) || (Array.isArray(a) && a.length !== 0));
                resolve(this.userData);
            })
        }));
    }

    getFzValues = () => {
        this.userData.forEach((data, i) => {
            if (data.length > 0) {
                let smoothedData = this.lowPassConstructor(data, ["Mx", "Fz", "My"], 30);
                // let peak = Math.max(...data.map(a => a["Fz"]));
                let peak = Math.max(...smoothedData["Fz"]);
                if (peak !== -Infinity) {
                    const startIndex = data.findIndex(d => d["Fz"] >= 4);

                    this.FzPeak[i] = peak;
                    this.FzPeakTime[i] = smoothedData.Fz.findIndex(a => a === peak) - startIndex;
                    //console.log("fz peak: " + this.FzPeakTime[i]/1000);

                    // this.FzPeakTime[i] = Number(data.find(b => b["Fz"] === peak)["key"]) - startIndex;
                    //console.log(this.FzPeakTime[i], i, data[startIndex-3].Fz);
                    let totalDistance = 0;
                    for (let v = startIndex + 20; v < startIndex + 5001; v++) {
                        if(v + 2 > data.length) break;
                        const copXP = (-smoothedData.Mx[v - 1]) / smoothedData.Fz[v - 1];
                        const copYP = (smoothedData.My[v - 1]) / smoothedData.Fz[v - 1];
                        const copX = (-smoothedData.Mx[v]) / smoothedData.Fz[v];
                        const copY = (smoothedData.My[v]) / smoothedData.Fz[v];

                        // // copX for current array position
                        // const copX = (-data[v].Mx) / data[v].Fz;
                        // // copY for current array position
                        // const copY = (data[v].My) / data[v].Fz;
                        // //copX for next array position
                        // const copXP = (-data[v + 1].Mx) / data[v + 1].Fz;
                        // //copY for next array position
                        // const copYP = (data[v + 1].My) / data[v + 1].Fz;

                        //                                   squareRoot((x2 - x1)^2           + (y2-y1)^2)
                        const hypot = Math.sqrt(Math.pow((copXP - copX), 2) + Math.pow(copYP-copY, 2));

                        //Distance between points added to counter
                        totalDistance += isNaN(hypot) ? 0 : hypot;
                    }
                    this.fetchID().then(res => {
                        this.COPLength[i] = totalDistance / (this.AthleteData.FOOTLENGTH / 100);
                        this.SAI[i] = (this.FzPeak[i] / (this.FzPeakTime[i]/1000)) / (this.AthleteData.BODYMASS*9.81);
                        // console.log(this.COPLength[i], this.SAI[i], this.userID, i);
                    });
                }
            }
        });

        //console.log(this.FzPeak, this.FzPeakTime, this.SAI);
    };

    collectFootData = () => {
        return new Promise(((resolve, reject) => {
            fetch(`${this.getIpAddress()}/sportData?footInfo=true&osu=${this.userID}`).catch(e => {
                console.log(e);
            }).then(results => {
                if (results.headers.get("content-type").includes("json")) {
                    return results.json();
                }
                else return null;
            }).then(e => {
                this.FootData = e;
                resolve(this.FootData);
            });
        }));
    };

    fetchDataCount = () => {
        return new Promise(((resolve, reject) => {
            if (this.userData.length > 0) {
                resolve(this.userData.length);
                return;
            }
            fetch(`${this.getIpAddress()}/sportData?countData=true&osu=${this.userID}`).catch(e => {
            }).then((results) => {
                return results.text();
            }).then(e => {
                this.userData = new Array(Number(e)).fill([]);
                resolve(e);
            });
        }))
    };

    fetchID = () => {
        return new Promise(((resolve, reject) => {
            if (this.AthleteData.BODYMASS === -1 || this.AthleteData.FOOTLENGTH === -1 || this.AthleteData.ID === -1) {
                fetch(`${this.getIpAddress()}/sportData?id=true&osu=${this.userID}`).catch(e => {
                    console.log(e)
                })
                    .then((results) => {
                        if (results === undefined) {
                            console.log("couldn't locate id file");
                        } else if (results.headers.get("content-type").includes("json")) {
                            return results.json();
                        }
                    }).then(data => {
                    this.AthleteData = data[0];
                });
            } else {
                resolve(this.AthleteData);
            }
        }));
    };

    fetchData = (count) => {
        return new Promise(((resolve, reject) => {
            let finalData = [];
            fetch(`${this.getIpAddress()}/sportData?number=${count}&osu=${this.userID}`).catch(e => {
                console.log(e);
            }).then((results) => {
                if (results === undefined) {
                    console.log(undefined);
                } else if (results.headers.get("content-type").includes("json")) {
                    return results.json().then((data) => {
                        let offset = {};
                        Object.keys(data[0]).forEach(key => {
                            offset[key] = data.slice(0, 50).map(a => a[key]).reduce((a, b) => a + b) / 50
                        });
                        data.forEach((files, index) => {
                            Object.keys(files).forEach((key) => {
                                files[key] = files[key]
                                    - offset[key];
                            });
                            files.key = index;

                            finalData.push(
                                files
                            );
                        });
                    });
                } else {
                    Promise.resolve(results.text()).then(a => {
                        if(a.toLowerCase().includes("invalid"))
                            reject("invalid number");
                    })
                }
            }).then(res => {
                if (finalData.length > 0) {
                    if (this.userData[count] !== null && this.userData[count].length === 0) {
                        this.userData[count] = finalData;
                        this.getFzValues();
                        resolve(finalData);
                    }
                }
            });
        }))
    };

    lowPassConstructor = (data, keys, freq = 30) => {
        let cons = {};

        keys.forEach((a, i) => {
            cons[a] = data.map(b => b[a]);
            lowPassFilter(cons[a], freq, 1000, 1);
        });

        return cons;
    };

}

export default User;