import React, {Component} from 'react';
import User from "./User";
import "./UserProfile.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import DataItem from "./DataItem";

class UserProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            toggledMenu: false,
            userChangeState: 0,
            userChangeMode: false,
            mousePosition: {
                x: 0,
                y: 0
            }
        };

        window.addEventListener('mousemove', this.onMouseMoveHandler);
    }

    componentDidMount() {
        if (this.props.user !== null)
            this.checkData();
    }

    onMouseMoveHandler = (e) => {
        this.setState({
            mousePosition: {
                x: e.pageX,
                y: e.pageY
            }
        })
    };

    checkData = () => {
        this.forceUpdate();
    };

    nameState = () => {
        switch (this.state.userChangeState) {
            case 1:
                return "Change User?";
            case 2:
                return "Return?";
            default:
                return this.props.user.userID;
        }
    };

    nameStateOnHover = () => {
        if (this.state.userChangeState === 0)
            this.setState({userChangeState: 1});
    };

    nameStateOnLeave = () => {
        if (this.state.userChangeState === 1)
            this.setState({userChangeState: 0});
    };

    changeUserOnClick = (value) => {
        this.setState({userChangeState: 0, userChangeMode: false});
        this.props.changeUser(this.props.frameID, value);
        setTimeout(this.checkData, 300);
    };

    usersListComponents = [];
    dataListComponents = [];

    populateList = () => {
        let dataSet = [];

        if (this.state.userChangeMode) {
            //User List
            this.usersListComponents = [];
            if (this.props.users.length !== this.usersListComponents.length) {
                this.props.users.forEach((value, index) => {
                    this.usersListComponents.push(<DataItem key={this.props.frameID + "-" + index}
                                                            mousePosition={this.state.mousePosition}
                                                            onClick={() => {
                                                                this.changeUserOnClick(value)
                                                            }}
                                                            containsDataElement={() => false}
                                                            text={value}/>);
                })
            }
            dataSet = this.usersListComponents;
        } else {
            //Data List
            this.dataListComponents = [];
            if (this.props.user.userData.length !== this.dataListComponents.length) {
                for (let index = 0; index < this.props.user.userData.length; index++) {
                    this.dataListComponents.push(<DataItem key={this.props.frameID + "-" + index}
                                                           mousePosition={this.state.mousePosition}
                                                           text={"Data Set: " + index + ((this.props.user && this.props.user.FootData.length > index && this.props.user.FootData[index].mistake === 0) ? " ": "-F")}
                                                           selectGraphContainer={() => this.props.selectGraphContainer(index)}
                                                           isDraggable={true}
                                                           containsDataElement={() => {return this.props.containsDataElement(index)}}
                                                           addToSelectedDataItems={() => this.props.addToSelectedDataItems(index)}
                                                           setSelectionMode={(x) => this.props.setSelectionMode(x)}
                        />
                    );
                }
            }
            dataSet = this.dataListComponents;
        }

        return (
            <ul className={"dataListContainer"}>
                {dataSet.map((value, index) => {
                    return value;
                })}
            </ul>
        )
    };

    triggerUserSwitch = () => {
        if (this.state.userChangeMode)
            this.setState({userChangeState: 0, userChangeMode: false});
        else
            this.setState({userChangeState: 2, userChangeMode: true});
    };

    userScreen = () => {
        if (this.props.user != null) {
            return (
                <div className="userProfileContainer">
                    <div className={"profileHead"} onMouseEnter={this.nameStateOnHover}
                         onMouseLeave={this.nameStateOnLeave}>
                        <p className={"profileTitle"} onClick={this.triggerUserSwitch}>{this.nameState()}</p>
                    </div>
                    <div className={"subProfileContainer"}>
                        <h4 className={"dataTitle"}>Data Sets</h4>
                        {this.populateList()}
                    </div>
                </div>
            );
        } else {
            return (
                <div className="userProfileContainer">
                    <div className={"profileHead"}>
                        <h1>Select Athlete</h1>
                    </div>
                </div>
            )
        }
    };

    render() {
        return this.userScreen()
    }
}

UserProfile.defaultProps = {
    user: new User(1755, "John Doe"),
    users: [],
    frameID: -1
};

export default UserProfile;