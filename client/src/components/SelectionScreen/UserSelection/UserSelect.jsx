import React, {Component} from 'react';
import UserProfile from "./UserProfile";
import "./UserSelect.css";

class UserSelect extends Component {

    constructor(props){
        super(props);
        this.state = {
            disable : false
        };
    }

    containsDataElement = (graphID, userID) => {
        if(this.props.selectedDataItems == null || this.props.selectedDataItems.length === 0) return false;

        return undefined !== this.props.selectedDataItems.find((a) => (a.graphID === graphID && a.userID === userID));
    };

    updateDataSets = () => {
        this.setState({disable : true});
        fetch(`${window.location.origin.toString().replace("3000", "9000")}/sportData?updateData=true`).catch(e => {
        }).then((results) => {
            return results.text();
        }).then(e => {
            setTimeout(() => {
                this.props.refreshData().then(() => {this.setState({disable : false});});
            }, 1000);
        });
    };

    render() {
        return (
            <div className={"userSelectContainer"}>
                <div className={"userSelectContainerHead"}>
                    <p className={"dataSelectionTitle"}>Data Selection</p>
                    <select onChange={(e) => {
                        this.props.setSelectedGraphID(e.nativeEvent.target.value.toString());
                    }} className={"form-control"}>
                        <option value="Fx">Fx</option>
                        <option value="Fy">Fy</option>
                        <option value="Fz">Fz</option>
                        <option value="Mx">Mx</option>
                        <option value="My">My</option>
                        <option value="Mz">Mz</option>
                        <option value="IN0">IN0</option>
                        <option value="IN1">IN1</option>
                        <option value="IN2">IN2</option>
                        <option value="IN3">IN3</option>
                        <option value="IN4">IN4</option>
                        <option value="IN5">IN5</option>
                        <option value="copX">copX</option>
                        <option value="copY">copY</option>
                        <option value="copXvY">copXvY</option>
                    </select>
                <button className={this.state.disable ? "btn btn-danger" : "btn btn-primary"} onClick={() => {this.updateDataSets()}} disabled={this.state.disable}>{this.state.disable ? "Loading..." : "Update DS"}</button>
                </div>
                <div className={"userContainer"}>
                    <UserProfile user={this.props.user1}
                                 users={this.props.users}
                                 changeUser={this.props.changeUser}
                                 frameID={1}
                                 setSelectionMode={(x) => this.props.setSelectionMode(x)}
                                 containsDataElement={(graphID) => {return this.containsDataElement(graphID, this.props.user1.userID)}}
                                 addToSelectedDataItems={(graphID) => {this.props.addToSelectedDataItems(graphID, this.props.user1.userID)}}
                                 selectGraphContainer={(graphID) => {this.props.selectGraphContainer(graphID, this.props.user1.userID)}}/>
                </div>
                <div className={"userContainer"}>
                    <UserProfile user={this.props.user2}
                                 users={this.props.users}
                                 changeUser={this.props.changeUser}
                                 frameID={2}
                                 setSelectionMode={(x) => this.props.setSelectionMode(x)}
                                 addToSelectedDataItems={(graphID) => this.props.addToSelectedDataItems(graphID, this.props.user2.userID)}
                                 containsDataElement={(graphID) => {return this.containsDataElement(graphID, this.props.user2.userID)}}
                                 selectGraphContainer={(graphID) => this.props.selectGraphContainer(graphID, this.props.user2.userID)}/>
                </div>
            </div>
        );
    }
}

UserSelect.defaultProps = {
    user1 : null,
    user2 : null,
    users : [],
    refreshData : new Promise(() =>{})
};

export default UserSelect;